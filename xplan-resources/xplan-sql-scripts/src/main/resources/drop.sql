---
-- #%L
-- xplan-workspaces - Modul zur Gruppierung aller Workspaces
-- %%
-- Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
\i fix/xplan40/drop.sql
\i fix/xplan41/drop.sql
\i fix/xplan50/drop.sql
\i fix/xplan51/drop.sql
\i fix/xplan52/drop.sql
\i fix/xplan53/drop.sql
\i fix/xplan54/drop.sql
\i fix/xplan60/drop.sql
\i fix/xplansyn/drop.sql
\i pre/xplan40/drop.sql
\i pre/xplan41/drop.sql
\i pre/xplan50/drop.sql
\i pre/xplan51/drop.sql
\i pre/xplan52/drop.sql
\i pre/xplan53/drop.sql
\i pre/xplan54/drop.sql
\i pre/xplan60/drop.sql
\i pre/xplansyn/drop.sql
\i archive/xplan40/drop.sql
\i archive/xplan41/drop.sql
\i archive/xplan50/drop.sql
\i archive/xplan51/drop.sql
\i archive/xplan52/drop.sql
\i archive/xplan53/drop.sql
\i archive/xplan54/drop.sql
\i archive/xplan60/drop.sql
\i archive/xplansyn/drop.sql
\i xplanmgr/drop.sql
