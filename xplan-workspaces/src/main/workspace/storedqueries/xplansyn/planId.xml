<!--
  #%L
  xplan-workspaces - Modul zur Gruppierung aller Workspaces
  %%
  Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<wfs:StoredQueryDefinition id="urn:ogc:def:query:OGC-WFS::PlanId" xmlns:wfs="http://www.opengis.net/wfs/2.0"
  xmlns:fes="http://www.opengis.net/fes/2.0" xmlns:xplan="http://www.deegree.org/xplanung/1/0">
  <wfs:Title>planId</wfs:Title>
  <wfs:Abstract>Es koennen vollstaendige XPlanGML Plaene anhand der PlanId heruntergeladen werden.</wfs:Abstract>
  <wfs:Parameter xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="planId" type="xsd:string">
    <Abstract>planId (Mandatory)</Abstract>
  </wfs:Parameter>
  <wfs:QueryExpressionText returnFeatureTypes="${deegreewfs:ServedFeatureTypes}"
    language="urn:ogc:def:queryLanguage:OGC-WFS::WFS_QueryExpression" isPrivate="false">

    <!-- BP_Plan -->
    <wfs:Query xmlns:gml="http://www.opengis.net/gml/3.2" typeNames="xplan:BP_Plan">
      <fes:Filter>
        <fes:And>
          <fes:PropertyIsEqualTo>
            <fes:ValueReference>
              //xplan:BP_Plan/xplan:nummer
            </fes:ValueReference>
            <fes:Literal>${planId}</fes:Literal>
          </fes:PropertyIsEqualTo>
          <fes:Or>
            <fes:PropertyIsLessThan>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsLessThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
          <fes:Or>
            <fes:PropertyIsGreaterThan>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsGreaterThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
        </fes:And>
      </fes:Filter>
    </wfs:Query>

    <!-- FP_Plan -->
    <wfs:Query xmlns:gml="http://www.opengis.net/gml/3.2" typeNames="xplan:FP_Plan">
      <fes:Filter>
        <fes:And>
          <fes:PropertyIsEqualTo>
            <fes:ValueReference>
              //xplan:FP_Plan/xplan:nummer
            </fes:ValueReference>
            <fes:Literal>${planId}</fes:Literal>
          </fes:PropertyIsEqualTo>
          <fes:Or>
            <fes:PropertyIsLessThan>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsLessThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
          <fes:Or>
            <fes:PropertyIsGreaterThan>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsGreaterThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
        </fes:And>
      </fes:Filter>
    </wfs:Query>

    <!-- LP_Plan -->
    <wfs:Query xmlns:gml="http://www.opengis.net/gml/3.2" typeNames="xplan:LP_Plan">
      <fes:Filter>
        <fes:And>
          <fes:PropertyIsEqualTo>
            <fes:ValueReference>
              //xplan:LP_Plan/xplan:nummer
            </fes:ValueReference>
            <fes:Literal>${planId}</fes:Literal>
          </fes:PropertyIsEqualTo>
          <fes:Or>
            <fes:PropertyIsLessThan>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsLessThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
          <fes:Or>
            <fes:PropertyIsGreaterThan>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsGreaterThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
        </fes:And>
      </fes:Filter>
    </wfs:Query>

    <!-- RP_Plan -->
    <wfs:Query xmlns:gml="http://www.opengis.net/gml/3.2" typeNames="xplan:RP_Plan">
      <fes:Filter>
        <fes:And>
          <fes:PropertyIsEqualTo>
            <fes:ValueReference>
              //xplan:RP_Plan/xplan:nummer
            </fes:ValueReference>
            <fes:Literal>${planId}</fes:Literal>
          </fes:PropertyIsEqualTo>
          <fes:Or>
            <fes:PropertyIsLessThan>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsLessThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitBeginn</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
          <fes:Or>
            <fes:PropertyIsGreaterThan>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
              <fes:Function name="now" />
            </fes:PropertyIsGreaterThan>
            <fes:PropertyIsNull>
              <fes:ValueReference>xplan:gueltigkeitEnde</fes:ValueReference>
            </fes:PropertyIsNull>
          </fes:Or>
        </fes:And>
      </fes:Filter>
    </wfs:Query>

  </wfs:QueryExpressionText>
</wfs:StoredQueryDefinition>
