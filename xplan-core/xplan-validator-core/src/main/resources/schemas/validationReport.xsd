<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  xplan-validator-core - XPlan Validator Core Komponente
  %%
  Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified"
           xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="ValidationReport" type="ValidationReportType"/>
  <xs:complexType name="ErrorsType">
    <xs:sequence>
      <xs:element type="xs:string" name="error" maxOccurs="unbounded" minOccurs="0"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MessagesType">
    <xs:sequence>
      <xs:element type="xs:string" name="message" maxOccurs="unbounded" minOccurs="0"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ValidationReportType">
    <xs:sequence>
      <xs:element type="xs:string" name="name"/>
      <xs:element type="xs:string" name="fileName"/>
      <xs:element type="xs:dateTime" name="date"/>
      <xs:element type="xs:boolean" name="isValid"/>
      <xs:element type="PlanType" name="Plan"/>
      <xs:element type="ExternalReferencesType" name="ExternalReferences"/>
      <xs:element type="ValidationType" name="Validation"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ExternalReferencesType">
    <xs:choice>
      <xs:element name="SkipMessage" type="xs:string"/>
      <xs:element name="ExternalReference" type="ExternalReferenceType" maxOccurs="unbounded"/>
    </xs:choice>
  </xs:complexType>
  <xs:complexType name="ExternalReferenceType">
    <xs:simpleContent>
      <xs:extension base="xs:string">
        <xs:attribute name="status" type="ExternalReferenceStatus">
        </xs:attribute>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:simpleType name="ExternalReferenceStatus">
    <xs:restriction base="xs:string">
      <xs:enumeration value="available" />
      <xs:enumeration value="missing" />
      <xs:enumeration value="unchecked" />
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="ValidationType">
    <xs:sequence>
      <xs:element type="SemType" name="Sem"/>
      <xs:element type="GeomType" name="Geom"/>
      <xs:element type="SynType" name="Syn"/>
      <xs:element type="SemType" name="Profile" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="RuleType">
    <xs:sequence>
      <xs:element type="xs:string" name="name"/>
      <xs:element type="xs:boolean" name="isValid"/>
      <xs:element type="xs:string" name="message"/>
      <xs:element type="InvalidFeaturesType" name="WarnedFeatures" minOccurs="0" maxOccurs="unbounded"/>
      <xs:element type="InvalidFeaturesType" name="ErroredFeatures" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="InvalidFeaturesType">
    <xs:sequence>
      <xs:element type="xs:string" name="message"/>
      <xs:element type="xs:string" name="gmlid" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="RulesType">
    <xs:sequence>
      <xs:element type="RuleType" name="Rule" maxOccurs="unbounded" minOccurs="0"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="WarningsType">
    <xs:sequence>
      <xs:element type="xs:string" name="warning" maxOccurs="unbounded" minOccurs="0"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SemType">
    <xs:sequence>
      <xs:element type="RulesMetadataType" name="RulesMetadata"/>
      <xs:element type="xs:string" name="result"/>
      <xs:element type="xs:string" name="details"/>
      <xs:element type="RulesType" name="Rules"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GeomType">
    <xs:sequence>
      <xs:element type="xs:string" name="result"/>
      <xs:element type="xs:string" name="details"/>
      <xs:element type="ErrorsType" name="Errors"/>
      <xs:element type="WarningsType" name="Warnings"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="SynType">
    <xs:sequence>
      <xs:element type="xs:string" name="result"/>
      <xs:element type="xs:string" name="details"/>
      <xs:element type="MessagesType" name="Messages"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="PlanType">
    <xs:sequence>
      <xs:element type="xs:string" name="name" maxOccurs="unbounded"/>
      <xs:element type="xs:string" name="id"/>
      <xs:element type="xs:string" name="version"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="RulesMetadataType">
    <xs:sequence>
      <xs:element type="xs:string" name="name"/>
      <xs:element type="xs:string" name="description"/>
      <xs:element type="xs:string" name="version"/>
      <xs:element type="xs:string" name="source"/>
    </xs:sequence>
  </xs:complexType>
</xs:schema>
