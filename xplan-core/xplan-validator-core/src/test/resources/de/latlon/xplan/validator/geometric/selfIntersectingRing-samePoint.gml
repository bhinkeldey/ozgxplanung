<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--
  #%L
  xplan-validator-core - XPlan Validator Core Komponente
  %%
  Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<gml:LinearRing xmlns:gml="http://www.opengis.net/gml/3.2" srsName="EPSG:31467" gml:id="GML_ID_67697">
  <gml:posList srsDimension="2" count="16">
    3566129.076 5934646.032
    3566132.864 5934616.273
    3566136.052 5934591.225
    3566082.863 5934589.344
    3566082.863 5934589.344
    3566079.720 5934614.057
    3566077.563 5934631.019
    3566129.076 5934646.032
  </gml:posList>
</gml:LinearRing>
