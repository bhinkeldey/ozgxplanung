<!--
  #%L
  xplan-validator-core - XPlan Validator Core Komponente
  %%
  Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<gml:Polygon xmlns:gml="http://www.opengis.net/gml/3.2" srsName="EPSG:25832"
             gml:id="GML_45ddaf05-73a8-42dd-b7ef-fe3549c449e1">
  <gml:exterior>
    <gml:LinearRing>
      <gml:posList>
        563743 5934846
        563398 5935060
        563129 5934541
        563257.8 5934463.8
        563433 5934363
        563750 5934472
        563743 5934846
      </gml:posList>
    </gml:LinearRing>
  </gml:exterior>
  <gml:interior>
    <gml:LinearRing>
      <gml:posList>
        563456.633379448 5934814.91196812
        563615.527392626 5934559.84526276
        563257.8 5934463.8
        563456.633379448 5934814.91196812
      </gml:posList>
    </gml:LinearRing>
  </gml:interior>
</gml:Polygon>
