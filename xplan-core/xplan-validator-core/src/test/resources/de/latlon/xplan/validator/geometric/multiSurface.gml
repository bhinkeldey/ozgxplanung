<!--
  #%L
  xplan-validator-core - XPlan Validator Core Komponente
  %%
  Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<gml:MultiSurface xmlns:gml="http://www.opengis.net/gml/3.2"  srsName="EPSG:25832" gml:id="GML_48d90d78-aa4a-44cc-939b-3562757993c6">
  <gml:surfaceMember>
    <gml:Polygon srsName="EPSG:25832" gml:id="GML_98a2ecff-1ec0-48e0-ad1f-fec708543420">
      <gml:exterior>
        <gml:LinearRing>
          <gml:posList srsDimension="2" count="5">73378.408 69717.162 73389.4 69736.9 73424.792 69745.504 73378.408 69745.504 73378.408 69717.162 </gml:posList>
        </gml:LinearRing>
      </gml:exterior>
    </gml:Polygon>
  </gml:surfaceMember>
  <gml:surfaceMember>
    <gml:Polygon srsName="EPSG:25832" gml:id="GML_b94fe35e-84e9-4ca9-9283-6d98938b4b04">
      <gml:exterior>
        <gml:LinearRing>
          <gml:posList srsDimension="2" count="5">73424.792 69687.760 73473.297 69687.760 73473.297 69717.162 73415.250 69727.228 73424.792 69687.760 </gml:posList>
        </gml:LinearRing>
      </gml:exterior>
    </gml:Polygon>
  </gml:surfaceMember>
</gml:MultiSurface>
