/*-
 * #%L
 * xplan-manager-web - Webanwendung des XPlan Managers
 * %%
 * Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.client.service;

import com.google.gwt.core.client.GWT;
import de.latlon.xplan.manager.web.shared.AuthorizationInfo;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * REST interface for security functions.
 *
 * @author <a href="mailto:stenger@lat-lon.de">Dirk Stenger</a>
 * @version $Revision: $, $Date: $
 */
public interface SecurityService extends RestService {

	public static class Util {

		private static SecurityService instance;

		public static SecurityService getService() {
			if (instance == null) {
				instance = GWT.create(SecurityService.class);
			}
			Resource resource = new Resource(GWT.getModuleBaseURL() + "rest/security");
			((RestServiceProxy) instance).setResource(resource);
			return instance;
		}

	}

	@GET
	@Produces(APPLICATION_JSON)
	@Path("/authorizationInfo")
	void retrieveAuthorizationInfo(MethodCallback<AuthorizationInfo> callback);

}
