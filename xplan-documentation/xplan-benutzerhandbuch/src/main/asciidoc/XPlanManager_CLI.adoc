[[xplanmanager-cli]]
=== XPlanManagerCLI

Die Komponente XPlanManagerCLI ist ein Kommandozeilenwerkzeug, welches
dem Fachadministrator der xPlanBox ermöglicht, die Datenhaltung zu
kontrollieren. Dabei ist das Kommandozeilenwerkzeug in der Lage,
XPlanGML-Dokumente in die Datenhaltung zu Laden, zu Löschen,
Listenausgaben zu erzeugen sowie Service-Metadaten für den XPlanWerkWMS zu erzeugen.

[[xplanmanager-cli-benutzungsanleitung]]
==== Benutzungsanleitung

Beim XPlanManagerCLI handelt es sich um ein Kommandozeilenwerkzeug, das
parametrisiert aufgerufen wird. Da diese Anwendung bei der Installation
in die PATH Variable aufgenommen wird, ist diese von einem beliebigen
Ort aufrufbar.

Das Basisverzeichnis mit dem Workspace `xplan-manager-workspace` muss über
die Umgebungsvariable _DEEGREE_WORKSPACE_ROOT_ gesetzt werden.

[[xplanmanager-cli-konfiguration-ueber-datei]]
===== Konfiguration über Datei

In dem Verzeichnis _<manager-cli-directory>/etc/_ befindet sich die
Konfigurationsdatei __managerConfiguration.properties__, welche genutzt
werden kann, um generelle Konfigurationen an dem Kommandozeilentool
durchzuführen.

Wird der Parameter `managerconfiguration` nicht angegeben, nutzt das Tool die unter
_etc/_ abgelegte Datei. Wenn der Parameter mitgegeben wird, muss sich die
Konfigurationsdatei in dem referenzierten Verzeichnis befinden.

[[xplanmanager-cli-hilfe]]
===== Hilfe

Die Hilfe mit den Angaben zu den möglichen Eingabeparametern lässt sich
mit dem Parameter `help` ausgeben.

Aufruf:

----
./XPlanManager --help
----

Ausgabe:

----
Usage: XPlanManager <options>

Usage: XPlanManager <options>

 --help
 --list [--managerconfiguration=<PFAD/ZU/VERZEICHNIS/MIT/MANAGERCONFIGURATION>]
 --import <xplanarchiv> [<xplanarchiv>..] [--force] [--crs=<CRS>] [--managerconfiguration=<PFAD/ZU/VERZEICHNIS/MIT/MANAGERCONFIGURATION>]
     <xplanarchiv> Die absolute oder relative Referenz auf den Plan, der importiert werden soll (verpflichtend). Mehrere Plaene koennen durch ein Leerzeichen getrennt angegeben werden.
     --force Erzwingen des Imports eines Plans mit Geomtriefehlern oder Validierungsfehlern (optional).EMPFOHLEN ist die Behebung der Fehler!
     --crs Angabe des Koordinatenreferenzsystems in dem die Daten vorliegen (optional).
 --export <planid> [<planid>..] [--target=<verzeichnis>] [--managerconfiguration=<PFAD/ZU/VERZEICHNIS/MIT/MANAGERCONFIGURATION>]
     <planid> Die ID des Plans der exportiert werden soll (verpflichtend). Mehrere IDs koennen durch ein Leerzeichen getrennt angegeben werden.
     --target Angabe des Verzeichnis in dem die exportierten XPlanArchive abgelegt werden sollen (optional).
 --delete <planid> [<planid>..]
     <planid> Die ID des Plans der gelöscht werden soll (verpflichtend). Mehrere IDs koennen durch ein Leerzeichen getrennt angegeben werden.
 --createMetadata <planid> [<planid>..] [--managerconfiguration=<PFAD/ZU/VERZEICHNIS/MIT/MANAGERCONFIGURATION>]
     <planid> Die ID des Plans zu dem der Service-Metadatensatz generiert werden soll (optional). Mehrere IDs koennen durch ein Leerzeichen getrennt angegeben werden. Wenn keine ID angegeben ist, werden für alle Plaene Metadatensaetze erstellt.

Allgemeine Parameter:
     --managerconfiguration Verzeichnis, in dem sich der Konfiguration des XPlanManagers befindet. Wenn die Option nicht angegeben wird, wird die Konfiguration aus dem Verzeichnis 'etc' des XPLanManagerCLIs verwendet.
     --v Ausgabe der Systemeigenschaften

Allgemeine Hinweise:
     Der Workspace `xplan-manager-workspace` muss im Verzeichnis _.deegree/_ des Home-Verzeichnis des Nutzers liegen, der das XPlanManagerCLI aufruft. Alternativ kann das Verzeichnis, in dem der Workspace liegt durch Angabe der Umgebungsvariablen _DEEGREE_WORKSPACE_ROOT_ gesetzt werden.

----


[[xplanmanager-cli-auflistung]]
===== Auflistung

Der Parameter `list` gibt die Liste der Pläne aus, die im XPlanManager importiert sind.

Aufruf:

----
./XPlanManager --list
----

Beispiel Ausgabe:

----
Id: 3, Version: XPLAN_54, Typ: FP_Plan, Name: Gesamtgeltungsbereich Flächennutzungsplan, Nummer: 12062024, GKZ: 12062024, Features: 2808, Importiert: 2022-02-18 17:57:11.669
Id: 6, Version: XPLAN_54, Typ: BP_Plan, Name: Lokstedt, Nummer: 56, GKZ: 02000000, Features: 251, Importiert: 2022-02-18 17:58:57.2
Id: 7, Version: XPLAN_53, Typ: BP_Plan, Name: Unbenannter Plan (3d099551-1f49-4b77-9008-68237a60426b), Nummer: -, GKZ: 4011000, Features: 351, Importiert: 2022-02-18 17:59:38.704
Id: 8, Version: XPLAN_52, Typ: BP_Plan, Name: Bebauungsplan 2135, Nummer: 2135, GKZ: 4011000, Features: 241, Importiert: 2022-02-18 18:00:45.077
Id: 9, Version: XPLAN_51, Typ: BP_Plan, Name: Bebauungsplan LA 22, Nummer: LA 22, GKZ: 1234567, Features: 146, Importiert: 2022-02-18 18:01:41.563
Id: 10, Version: XPLAN_41, Typ: RP_Plan, Name: Regionalplanbeispiel Bereich Siedlungsstruktur, Nummer: Siedlungsstruktur 1, Features: 282, Importiert: 2022-02-18 18:02:25.616
Id: 11, Version: XPLAN_40, Typ: LP_Plan, Name: Landschaftsplan 16 Gemeinde XYZ", Nummer: 16, Features: 1659, Importiert: 2022-02-18 18:03:22.091
Id: 12, Version: XPLAN_53, Typ: BP_Plan, Name: Bebauungsplan LA 22, Nummer: LA 22, GKZ: 02000000, Features: 1350, Importiert: 2022-02-18 21:16:06.753
Anzahl Pläne: 8
----

[[xplanmanager-cli-import]]
===== Import

Ein Import kann durch Angabe des Parameters `import` gefolgt vom Pfad
zum XPlanArchiv angestoßen werden. Bei dem Import können
XPlanGML-Vektordaten und XPlanGML-Rasterdaten ohne zusätzlichen
Parameter in die Datenhaltung geladen werden. Mehrere Pläne können durch Leerzeichen getrennt angegeben werden.

Beispiel Aufruf:

----
./XPlanManager --import ../Input-Planverzeichnis/Infrastruktur.zip
----

Während des Imports finden zahlreiche Konsistenz- und
Korrektheitsüberprüfungen statt. Dies betrifft u.a. Schemavalidität,
Geometrievalidität, Korrektheit von Links, Angabe von
Koordinatenreferenzsystemen, u.v.m.

Folgende Parameter können mit angegeben werden, um z. B. fehlende
Informationen anzugeben.

----
--force
--crs=EPSG:25832
--managerconfiguration=/PFAD/ZUR/MANAGER/KONFIGURATION
----

_--force_

Enthält das XPlanGML-Dokument Geometriefehler o.ä., ist es
dringend angeraten, diese vor einem Import zu bereinigen. Der Import eines Planes kann mit dem Parameter `force` erzwungen werden.

CAUTION: Bitte beachten Sie, dass dabei vorliegenden Geometriefehler o.ä. übernommen werden und der importierte Plan dadurch fehlerhaft ist. Die
Auswirkungen können von einer fehlerhaften Darstellung des Plans bis hin zu unerwarteten Verhalten der xPlanBox reichen.

----
./XPlanManager --import --force ../Input-Planverzeichnis/Infrastruktur.zip
----

_-–crs_

Fehlt die Angabe des Koordinatenreferenzsystems in den Daten, so kann
dieses mit dem Parameter `crs` übergeben werden.

Beispiel Aufruf:

----
./XPlanManager --import ../Input-Planverzeichnis/Infrastruktur.zip --crs=EPSG:31467
----

_-–managerconfiguration_

Soll eine vorhandene Konfigurationen für den XPlanManager verwendet werden, so kann durch den
Parameter `managerconfiguration` die entsprechende Konfiguration
übergeben werden. Andernfalls wird die Konfiguration im Verzeichnis `etc` des XPlanManagerCLIs verwendet.

----
./XPlanManager --import ../Input-Planverzeichnis/Infrastruktur.zip --managerconfiguration=<XPLANBOX_CONFIG>
----

Beispiel Ausgabe für erfolgreichen Import

----
Analyse des XPlanArchivs
('../../resources/testdata/XPlanGML_3_0/Infrastruktur.zip')...OK.
- Analyse des Dokuments...OK [1167 ms]: XPLAN_53, RP_Plan, EPSG:31466
- Schema-Validierung...OK [5135 ms]
- Einlesen der Features (+ Geometrievalidierung)...OK [6486 ms]: 492 Features
...
- Überprüfung der XLink-Integrität...OK [3 ms]
- Überprüfung der externen Referenzen...OK [1 ms]
- Erzeugen der XPlan-Syn Features...Keine Beschreibung für externen Code 'RpTextDefaultSymbol' (CodeList XP_StylesheetListe) gefunden. Verwende Code als Beschreibung. Keine Beschreibung für externen Code 'RpTextDefaultSymbol' (CodeList XP_StylesheetListe) gefunden. Verwende Code als Beschreibung.
...
OK [6376 ms]
- Einfügen der Features in den FeatureStore (XPLAN_53)...OK [9873 ms].
- Einfügen der Features in den FeatureStore (XPLAN_SYN)...OK [9217 ms].
- Einfügen in Manager-DB...OK [49 ms].
- Einfügen von Plan-Artefakt 'xplan.gml'...OK.
- Persistierung...OK [109 ms].
Plan wurde eingefügt. Zugewiesene Id: 13
----

[[xplanmanager-cli-rasterdatenanalyse]]
===== Rasterdatenanalyse

Die Rasterdaten werden beim Import auf Nutzbarkeit überprüft werden,
damit sichergestellt ist, dass diese korrekt in den XPlanWMS
eingebettet werden können.
Die Prüfung beinhaltet das CRS des Rasterplans, sowie das Format.

Beispiel Aufruf:

----
./XPlanManager --import ~/test-data/BPlan002_5-2.zip --managerconfiguration=<XPLANBOX_CONFIG>
----

Beispiel Ausgabe:

----
Evaluationsergebniss von referenzierten Rasterdaten:
  - Name: B-Plan_Klingmuehl_Heideweg_Karte.tif Unterstütztes CRS: Ja Unterstütztes Bildformat: Ja
Es existieren keine invaliden Rasterdaten
- Einlesen der Features (+ Geometrievalidierung)...OK [839 ms]: 500 Features
- Überprüfung der XLink-Integrität...OK [2 ms]

- Erzeugen/Einsortieren der Rasterkonfigurationen (Veröffentlichungsdatum: 01.02.2002)...Succeeding plan id: null
73_B-Plan_Klingmuehl_Heideweg_Karte
77_B-Plan_Klingmuehl_Heideweg_Karte
79_B-Plan_Klingmuehl_Heideweg_Karte
OK [1591 ms]

Rasterscans:
 - B-Plan_Klingmuehl_Heideweg_Karte.tif
WMS Konfiguration für Id 79 nach /home/user/.deegree/xplansyn-wms-workspace geschrieben.
XPlanArchiv wurde erfolgreich importiert. Zugewiesene Id: 79
----

Passt das CRS der Rasterdaten nicht mit dem CRS der Rasterdatenhaltung überein, so
erhält der Nutzer die Option, den Plan ohne Erzeugung der
Rasterkonfiguration zu importieren:

----
Evaluationsergebniss von referenzierten Rasterdaten:
  - Name: Abrundungssatzung_Gruhno_ergb.tif Unterstütztes CRS: Kein Unterstütztes Bildformat: Ja
Aufgrund invalider Rasterdaten wird der Import abgebrochen. Sie können den Import ohne die Erzeugung von Rasterkonfigurationen erzwingen, indem Sie die Option --force angeben.
----

[[xplanmanager-cli-export]]
===== Export

Der Export eines Planes erfolgt unter Angabe des Parameters `export`
gefolgt von der PlanID (diese kann zuvor mit dem Parameter `list` herausgefunden werden)
und dem Ausgabeverzeichnis. Mehrere PlanIDs können durch Leerzeichen getrennt angegeben werden.

Beispiel Aufruf:

----
./XPlanManager --export 9 --target=outputverzeichnis
----

Beispiel Ausgabe für erfolgreichen Export:

----
- Schreibe Artefakt 'xplan.gml'...OK.
Plan 9 wurde nach 'xplan-exported-9.zip' exportiert.
----

[[xplanmanager-cli-loeschen]]
===== Löschen

Beim Löschen wird dem Parameter `delete` die PlanID (diese kann zuvor mit
`list` herausgefunden werden) übergeben. Mehrere PlanIDs können durch Leerzeichen getrennt angegeben werden.

Beispiel Aufruf:

----
./XPlanManager --delete 1
----

Beispiel Ausgabe:

----
- Entferne Plan 1 aus dem FeatureStore (XPLAN_53)...OK
- Entferne Plan 1 aus dem FeatureStore (XPLAN_SYN)...OK
- Entferne Plan 1 aus der Manager-DB...OK
- Persistierung...OK
Plan 1 wurde gelöscht.
----

[[xplanmanager-metadaten-erzeugen]]
===== Erzeugen von Service-Metadatensätzen

Mit dieser Option können Metadatensätze für den XPlanWerkWMS erstellt werden. Bei der Erstellung der Informationen für die Capabilities des XPlanWerkWMS werden dabei bereits vorhandene Informationen überschrieben. Generierte Service-Metadatensätze werden nicht überschrieben, sondern können anhand des Zeitstempels im Dateinamen dem Zeitpunkt der Erstellung zugeordnet werden. Es wird jedoch ein neuer FileIdentifier generiert.
Für einzelne Pläne können Metadatensätze durch Angabe der PlanID (diese kann zuvor mit `list` herausgefunden werden) erzeugt werden. Mehrere PlanIDs können durch Leerzeichen getrennt angegeben werden. Wird keine PlanID angegeben, werden die Metadatensätze für alle Pläne erzeugt.

Beispiel Aufruf:

----
./XPlanManager --createMetadata 1
----

[[xplanmanager-cli-troubleshooting]]
===== Troubleshooting

Beim Import sehr großer Archive, kann es zu einem _OutOfMemoryError_
Laufzeitfehler kommen, da die Java Virtual Machine keinen weiteren
freien Speicher allokieren kann. Wenn der Server noch über freien
Arbeitsspeicher verfügt, dann kann dieser über die Umgebungsvariable
`JAVA_OPTS` unter Linux wie folgt erhöht werden:

----
export JAVA_OPTS='-Xmx4096m'
----

Weitere Informationen zur Konfiguration des Servers im Kapitel
<<bekannte-probleme,Bekannte Probleme - Kapazitätsbezogene Einschränkungen>> und
im Betriebshandbuch.
