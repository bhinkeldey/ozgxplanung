[[verfahrensdatenbank]]
=== Schnittstelle zur Verfahrensdatenbank (optional)

Um beim Import eines Plans die Verfahrens-Id aus einer bereits vorhandenen Verfahrensdatenbank abzurufen, muss die Datenbankverbindung zur Verfahrensdatenbank konfiguriert und aktiviert werden.

Der Dialog im XPlanManagerWeb kann in der Datei _<XPLANBOX_CONFIG>/managerWebConfiguration.properties_ aktiviert oder deaktiviert werden mit:

[source,properties]
----
activateInternalIdDialog=[true|false]
----

Die Datenbankverbindung zu einer Verfahrensdatenbank und die SQL-Abfragen zur Abfrage der Verfahrens-Id erfolgt in der Datei _<XPLANBOX_CONFIG>/managerConfiguration.properties_:

.Beispiel für _managerConfiguration.properties_ mit SQL-Abfragen:
[source,properties]
----
workspaceName=xplan-manager-workspace <1>
jdbcConnectionId=vfdb <2>
internalIdLabel=verfahrensid <3>
internalNameLabel=verfahrensname <4>
selectMatchingIdsSql=SELECT verfahrensid, verfahrensname FROM planverfahren WHERE lower(verfahrensname) LIKE lower(?) ORDER BY verfahrensname ASC <5>
selectAllIdsSql=SELECT verfahrensid, verfahrensname FROM planverfahren ORDER BY verfahrensname ASC <6>
----
<1> _workspaceName_: Angabe des deegree Workspace in der die Datenbankverbindung zur Verfahrensdatenbank konfiguriert ist
<2> _jdbcConnectionId_: Name der Konfigurationsdatei mit der Datenbankverbindung (ohne die Dateiendung _.xml_)
<3> _internalIdLabel_: Tabellenspalte in der die Verfahrens-Id hinterlegt ist
<4> _internalNameLabel_: Tabellenspalte in der der Verfahrensname hinterlegt ist. Die beiden Spaltennamen aus <3> und <4> müssen in den beiden SQL-Statements <5> und <6>
selektiert werden.
<5> _selectMatchingIdsSql_: SQL-Statement mit der Abfrage der Verfahren, die dem Plan über den Plan-Namen zugeordnet sind. Der Plan-Name wird an der Stelle des Platzhalters '?' eingesetzt. Das SQL-Statement muss die mit _internalIdLabel_ und _internalNameLabel_ angegeben Spalten selektierten und exakt einen Platzhalten '?' für den Plan-Namen beinhalten.
<6> _selectAllIdsSql_: Das SQL-Statement mit der Abfrage aller vorhandenen Verfahren. Das SQL-Statement muss die mit _internalIdLabel_ und _internalNameLabel_ angegeben Spalten selektierten und darf keinen Platzhalter '?' beinhalten.

Die Datenbankverbindung kann in der Datei _vfdb.xml_ im XPlanManagerWorkspace konfiguriert werden (der Name des workspaces und die ID der
Konfigurationsdatei können wie zuvor beschrieben geändert werden):

.Beispiel für _vfdb.xml_ mit SQL-Abfragen:
[source,xml]
----
<DataSourceConnectionProvider xmlns="http://www.deegree.org/connectionprovider/datasource"
                              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                              xsi:schemaLocation="http://www.deegree.org/connectionprovider/datasource https://schemas.deegree.org/core/3.5/connectionprovider/datasource/datasource.xsd">

  <DataSource javaClass="org.apache.commons.dbcp2.BasicDataSource" destroyMethod="close"/>

  <!-- Configuration of DataSource properties -->
  <Property name="driverClassName" value="org.postgresql.Driver"/>
  <Property name="url" value="jdbc:postgresql://<host>:<port>/<dbname>"/>
  <Property name="username" value="<USERNAME>"/>
  <Property name="password" value="<PASSWORD>"/>
  <Property name="initialSize" value="1"/>
  <Property name="maxTotal" value="10"/>
  <Property name="maxIdle" value="1"/>
  <Property name="maxWaitMillis" value="5000"/>
  <Property name="testOnReturn" value="true"/>
  <Property name="testOnBorrow" value="true"/>
  <Property name="validationQuery" value="select 1"/>

</DataSourceConnectionProvider>
----

IMPORTANT: Die Schnittstelle zur Verfahrensdatenbank ist veraltet und wird in zukünftigen Versionen der xPlanBox entfernt.