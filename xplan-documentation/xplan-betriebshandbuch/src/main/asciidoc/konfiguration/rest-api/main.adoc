[[konfiguration-rest-api]]
== Konfiguration der REST-Schnittstellen

[[konfiguration-manager-api]]
=== XPlanManagerAPI

Die Konfiguration der XPlanManagerAPI erfolgt in der Datei _<XPLANBOX_CONFIG>/managerApiConfiguration.properties_.
Außerdem gelten die im Abschnitt <<konfiguration-manager>> beschriebenen Konfigurationsoptionen, die in der Datei _<XPLANBOX_CONFIG>/managerConfiguration.properties_ festgelegt werden.

Folgende Konfigurationen können für die XPlanManagerAPI vorgenommen werden:

==== API URL

URL der XPlanManagerAPI, die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration muss vorgenommen werden, andernfalls kann die Schnittstelle nicht verwendet werden und es kommt bereits beim Deployment der Webapp zu einem Fehler.
Beispiel:

----
apiUrl=https://xplanbox.lat-lon.de
----

==== Kontakt E-Mail Adresse

E-Mail Adresse des Kontakts, die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
contactEMailAddress=info@lat-lon.de
----

==== URL zu den "Terms of Service"

URL zu den "Terms of Services", die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
termsOfServiceUrl=https://opensource.org/tos/
----

==== URL zur Dokumentation der xPlanBox

URL zur Dokumentation, die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
documentationUrl=https://xplanbox.lat-lon.de/xPlanBox-Benutzerhandbuch/
----

==== WMS URL

URL des XPlanwerkWMS, über den einzelne Pläne über die ManagerID angefragt werden können.
Beispiel:

----
wmsUrl=http://xplanbox.lat-lon.de/xplan-wms
----

==== Standard-Validierungseinstellungen

Mit diesen Einstellungen kann das Validierungslevel konfiguriert werden, das beim Import eines Plans angewendet wird.
Standardmäßig müssen alle Tests (syntaktisch, semantisch und geometrisch) bestanden werden.
Mit dieser Option kann z. B. festgelegt werden, dass Pläne die Prüfung auf den Geltungsbereich nicht bestehen müssen, um diesen zu importieren.
Unabhängig von dieser Konfiguration muss die syntaktische Validierung in jedem Fall bestanden werden, ebenso werden Pläne mit schwerwiegenden geometrischen Fehlern grundsätzlich abgelehnt.

Beispiel:

----
skipSemantisch=false
skipGeometrisch=false
skipFlaechenschluss=false
skipLaufrichtung=false
skipGeltungsbereich=true
----

[[konfiguration-validator-api]]
=== XPlanValidatorAPI

Die Konfiguration der XPlanValidatorAPI erfolgt in der Datei _<XPLANBOX_CONFIG>/validatorApiConfiguration.properties_.
Außerdem gelten die im Abschnitt <<konfiguration-validator>> beschriebenen Konfigurationsoptionen, die in der Datei _<XPLANBOX_CONFIG>/validatorConfiguration.properties_ festgelegt werden.
Folgende Konfiguration kann für die XPlanValidatorAPI vorgenommen werden:

==== API URL

URL der XPlanManagerAPI, die im OpenAPI Dokument ausgegeben wird.
Beispiel:

----
apiUrl=https://xplanbox.lat-lon.de
----

==== Kontakt E-Mail Adresse

E-Mail Adresse des Kontakts, die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
contactEMailAddress=info@lat-lon.de
----

==== URL zu den "Terms of Service"

URL zu den "Terms of Services", die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
termsOfServiceUrl=https://opensource.org/tos/
----

==== URL zur Dokumentation der xPLanBox

URL zur Dokumentation, die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
documentationUrl=https://xplanbox.lat-lon.de/xPlanBox-Benutzerhandbuch/
----

[[konfiguration-document-api]]
=== XPlanDokumentenAPI
Die Konfiguration der XPlanDokumentenAPI erfolgt in der Datei _<XPLANBOX_CONFIG>/dokumentenApiConfiguration.properties_.

Folgende Konfiguration kann für die XPlanDokumentenAPI vorgenommen werden.

==== API URL

URL der XPlanDokumentenAPI, die im OpenAPI Dokument ausgegeben wird.
Beispiel:

----
apiUrl=https://xplanbox.lat-lon.de
----
Wird keine URL angegeben, dann wird eine relative URL ausgegeben

==== Kontakt E-Mail Adresse

E-Mail Adresse des Kontakts, die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
contactEMailAddress=info@lat-lon.de
----

==== URL zu den "Terms of Service"

URL zu den "Terms of Services", die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
termsOfServiceUrl=https://opensource.org/tos/
----

==== URL zur Dokumentation der xPLanBox

URL zur Dokumentation, die im OpenAPI Dokument ausgegeben wird. Diese Konfiguration ist optional.
Beispiel:

----
documentationUrl=https://xplanbox.lat-lon.de/xPlanBox-Benutzerhandbuch/
----
