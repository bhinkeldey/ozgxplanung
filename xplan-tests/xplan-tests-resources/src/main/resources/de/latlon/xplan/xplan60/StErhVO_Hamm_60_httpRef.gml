<?xml version="1.0" encoding="utf-8" standalone="yes"?>

<!--
Testplan für Version 6.0, der folgende Neuerungen beinhaltet: 
Neue Relation texte (XP_TextAbschnitt) (CR-058)
-->
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xplan="http://www.xplanung.de/xplangml/6/0" gml:id="Gml_7DE047B2-E177-421F-AFFB-0773D3617651" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 http://www.xplanungwiki.de/upload/XPlanGML/6.0/Schema/XPlanung-Operationen.xsd">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>569230.4272 5933929.5049</gml:lowerCorner>
      <gml:upperCorner>570727.964 5935766.084</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:SO_Plan gml:id="Gml_1A8DF387-B40C-4E65-9B58-2C2BC092249C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>569230.4272 5933929.5049</gml:lowerCorner>
          <gml:upperCorner>570727.964 5935766.084</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>StErhVO_Hamm_Testplan_60</xplan:name>
      <xplan:beschreibung>siehe Lageplan</xplan:beschreibung>
      <xplan:genehmigungsDatum>2021-01-18</xplan:genehmigungsDatum>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_A5A08B12-D62C-442B-94D2-D5D3819170AF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F7FC7F40-C692-42E3-84D9-B8755116B476" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570416.681 5934296.146 570413.1171 5934290.6619 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EE170D43-9BCD-4F6F-8169-B9CF81689BB1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570413.1171 5934290.6619 570227.2469 5934323.7349 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_23741525-0936-416F-8A22-C5B17B405A29" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570227.2469 5934323.7349 570173.0072 5934215.2555 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B23F517A-3359-4B4E-B4BF-49E29E443449" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570173.0072 5934215.2555 570127.3665 5934223.8544 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B81E3EC6-C980-4FD0-93C1-78EE9087DB43" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570127.3665 5934223.8544 570113.063 5934192.394 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_12A7220C-D032-4F01-8DB1-5E93A9011F2F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570113.063 5934192.394 570073.533 5934112.959 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_47795C7D-FDE7-4BBE-A7FC-C0A61036978C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570073.533 5934112.959 570067.1737 5934098.8386 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8AE62B73-C87A-4F1D-BDF2-26089D0FDCEF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570067.1737 5934098.8386 569824.418 5934145.1408 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_90C2407F-B11B-4831-925F-DE4EACB8C4E7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569824.418 5934145.1408 569782.746 5934058.4895 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_51FA8D99-A71F-4395-B28F-7A962CEF8154" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569782.746 5934058.4895 570495.7995 5933929.5049 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8BE1229B-2242-4220-8BA8-AF2A06C105E5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570495.7995 5933929.5049 570553.3465 5934012.1874 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1DA3560B-F1CF-4AEB-B74A-AB69E5DFB744" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570553.3465 5934012.1874 570689.6072 5933993.005 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E835688F-D7B2-406E-BB2C-21847C6DFBB4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570689.6072 5933993.005 570710.7739 5934180.1981 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F7C7C11C-72EA-4F7B-ABF2-4462CAFE7CAF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570710.7739 5934180.1981 570715.4041 5934237.7451 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D22748D3-24E7-4F56-832A-CB7435767216" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570715.4041 5934237.7451 570719.3729 5934252.9587 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_47716E4B-0B0F-4B69-AD6E-A4AE0413A953" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570719.3729 5934252.9587 570708.1281 5934352.1776 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_177EEAA3-AF2E-4556-8DEC-8CC1A12614E7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570708.1281 5934352.1776 570727.964 5934394.294 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E4E245C-D028-42CF-83BA-B819461A207D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570727.964 5934394.294 570725.183 5934419.353 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7735B1E0-3B5B-4FEC-A9A9-AB632C40EA1C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570725.183 5934419.353 570722.749 5934419.242 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_517A8F83-FBBF-446F-8EA7-1F4939548E89" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570722.749 5934419.242 570722.436 5934421.973 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F15543B-9340-498C-B5A2-244C992B9C22" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570722.436 5934421.973 570720.021 5934443.076 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_57F9B320-D2EC-4CBF-ACCF-6B360D11CA78" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570720.021 5934443.076 570719.593 5934446.813 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3868BBF5-1F19-4803-9304-99BA6F4B6DEB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570719.593 5934446.813 570717.285 5934449.018 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F33E12B-E628-4E50-B765-AC2B3D9464B0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570717.285 5934449.018 570709.556 5934507.593 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E8191CC5-6B76-4580-B9E8-31890EBE0682" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570709.556 5934507.593 570699.09 5934586.865 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2CE87AEE-1B4C-4941-A351-8934CA0E2F80" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570699.09 5934586.865 570696.699 5934606.715 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CA86F1CC-BEC6-4D41-89D3-514AA0B615F7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570696.699 5934606.715 570682.5221 5934724.3021 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ACD4A105-1B69-4877-94C8-596D8C4B966E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570682.5221 5934724.3021 570663.668 5934880.6839 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_60F55E8E-D78D-4FCE-A9AA-4E477895A811" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570663.668 5934880.6839 570652.427 5934973.92 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40EAA838-8F6F-4F58-B7AB-878AEBCB61E5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570652.427 5934973.92 570651.9607 5934977.4523 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FAC9CA96-5017-4D81-B017-42EC67362ACD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570651.9607 5934977.4523 570647.911 5935008.13 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BD00703E-6C75-4DA1-BE0F-7FECD6289CED" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570647.911 5935008.13 570644.512 5935023.756 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B6A8F6A0-80C3-4423-9415-190DC81DC358" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570644.512 5935023.756 570631.979 5935045.949 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E2E54EF4-2390-4DDA-B16F-006B2B24826C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570631.979 5935045.949 570617.25 5935084.336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_404A03A9-A49A-416F-A3AA-59D87384817F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570617.25 5935084.336 570610.8745 5935100.95 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3C77DB4D-1FF8-4ACC-B58C-0FFC7016246F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570610.8745 5935100.95 570501.699 5935385.449 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D77ACA05-E446-4E00-9525-E30ED73BD602" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570501.699 5935385.449 570473.004 5935460.231 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1A2C364E-C957-4A84-A05A-D03A12D2EB4B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570473.004 5935460.231 570398.174 5935648.743 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8899C627-4EEF-4C51-AB0C-A67DE32B60BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570398.174 5935648.743 570389.877 5935669.644 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C7CECCD9-D55A-4C9C-950E-DD3699A1947C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570389.877 5935669.644 570352.505 5935763.753 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0F6F0AD9-D2AA-4F46-B856-FC5BC3868F0A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570352.505 5935763.753 570351.579 5935766.084 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0D168EF0-EE7E-4A44-B08C-3F9908263A83" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570351.579 5935766.084 570305.964 5935743.357 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_34E4D279-7E67-442D-8673-BAABCA703777" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570305.964 5935743.357 570241.835 5935711.403 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_14FDD77D-0A80-49B1-AA59-220B733D36FA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570241.835 5935711.403 570240.99 5935717.756 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B86374D7-1292-4424-A7C6-89EC96D5915C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570240.99 5935717.756 570224.815 5935712.291 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E369CBF5-334B-4152-B80D-DE813305E6AE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570224.815 5935712.291 570183.682 5935693.091 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B0CB64CC-CAF3-4108-899C-4B46F0CC78BA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570183.682 5935693.091 570145.609 5935677.461 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2684028D-D7CC-4AF9-81BA-E7CE214C962C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570145.609 5935677.461 570111.089 5935663.444 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_60F02C4E-F338-4310-886C-B7A0CC616531" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570111.089 5935663.444 570111.248 5935661.853 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6FC373C4-9865-45C2-8431-0BEA6F475F8F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570111.248 5935661.853 570062.741 5935640.65 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_97FCA824-87E2-4DCD-AB3F-5363B0741FFB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570062.741 5935640.65 570047.866 5935634.148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C1672D36-755A-486F-AE3A-9E8B8CF9A23B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570047.866 5935634.148 570047.439 5935637.031 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3AABE986-15EC-4848-81A7-F7F64197B5FA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570047.439 5935637.031 570043.259 5935635.114 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_198283D6-8496-4ECD-9958-3A6B56F50A22" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570043.259 5935635.114 570042.825 5935638.214 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_31647D12-25A6-4963-906E-4254778106CA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570042.825 5935638.214 570040.0538 5935636.943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_41B73997-872B-443D-BF02-D966D87AB546" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570040.0538 5935636.943 570028.402 5935631.599 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D6C0FB66-0E53-44A9-9DE6-15EA3A9DE154" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570028.402 5935631.599 570022.267 5935628.786 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_33B6A494-EA3D-4666-A365-B0EF9483BB80" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570022.267 5935628.786 570021.398 5935620.08 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B2FF03BF-7DC9-4910-9AEE-4F2335547811" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570021.398 5935620.08 570019.966 5935619.369 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_91A25093-0460-46AB-84DD-CDE0D890DF8F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570019.966 5935619.369 570015.343 5935617.078 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C5E093D2-7F2D-4837-84B9-B3C29E3850EB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570015.343 5935617.078 570005.198 5935612.047 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A21F641-EACA-4CCE-B0BA-BB7DE4F0D8A3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570005.198 5935612.047 569987.794 5935604.094 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E7D849B2-0F17-46AB-9212-AD71603FE6A2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569987.794 5935604.094 569978.59 5935599.889 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A54B8DFE-FF06-445B-A349-DEC177DA7679" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569978.59 5935599.889 569964.257 5935593.341 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6B6393DC-48AE-471E-84E7-BC78A3F04681" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569964.257 5935593.341 569957.926 5935590.835 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0761FD7E-0918-420D-8B1A-E2FB0CD0DD7A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569957.926 5935590.835 569927.016 5935578.592 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5C5DE230-8A1F-4A12-8172-498BF9EE0997" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569927.016 5935578.592 569898.702 5935567.541 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_55706FD6-0CEB-4DC3-ACD7-C361E070C35D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569898.702 5935567.541 569889.65 5935564.007 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A6D82367-C99E-4EAA-B773-5022C7AA67C1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569889.65 5935564.007 569852.189 5935549.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_98F319BF-8D0A-4C38-891D-C8878CFC4939" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569852.189 5935549.725 569815.032 5935534.788 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B2FB3AA8-F1BB-45A3-B3AF-2A271FF182A5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569815.032 5935534.788 569809.8659 5935533.0967 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_748A912B-4023-46AE-810E-3BB34850C8B8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569809.8659 5935533.0967 569802.487 5935530.681 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DAB66B05-9642-4F3B-84F3-2975EA6992F1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569802.487 5935530.681 569782.3991 5935524.1047 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_616E3351-F019-47C1-92D3-FC0900162FE9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569782.3991 5935524.1047 569757.725 5935516.027 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E5148084-7E5B-4508-BBEA-C2F2036D0264" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569757.725 5935516.027 569707.595 5935502.147 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1AE46EA1-569E-443A-8B51-475AC892EDB6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569707.595 5935502.147 569682.885 5935497.12 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_12B42FDD-5288-4895-B33E-FEEB1C88D9BD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569682.885 5935497.12 569674.348 5935495.002 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3A6CAC2C-F2C9-48AA-8757-33BA0B4EAFE6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569674.348 5935495.002 569654.498 5935490.799 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5EA1D73A-4C2E-4561-A6EB-61B70EFFA351" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569654.498 5935490.799 569635.411 5935487.319 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A20C2E7-4C65-4D31-A5DF-9E9C2C709EC8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569635.411 5935487.319 569635.2405 5935487.2883 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9E435221-8980-41AB-ACB2-02B183E1B5CF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569635.2405 5935487.2883 569625.5071 5935485.5347 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B9E33EA2-D8C5-4498-AE32-1F0CD1D6D4D3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569625.5071 5935485.5347 569625.3186 5935485.9195 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6C8CF886-ADB4-4604-938F-D031E952D226" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569625.3186 5935485.9195 569616.7196 5935425.7266 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3F379E96-42F1-4522-9CED-44A9183B2AE0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569616.7196 5935425.7266 569422.912 5935401.9141 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E417E271-FB8D-47E8-9F17-532AFBDF55AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569422.912 5935401.9141 569416.528 5935460.741 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1D76C7CF-2A2C-4FBB-B655-368F94622166" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569416.528 5935460.741 569383.741 5935457.115 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_188937D6-CD2C-4223-B551-45035F17D34E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569383.741 5935457.115 569391.8234 5935395.961 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_268C1E30-E066-4B2F-9601-F9225B867C48" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569391.8234 5935395.961 569337.5837 5935389.3464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_72796519-F221-4B41-883F-B8DE06C6E420" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569337.5837 5935389.3464 569263.5002 5935374.7943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A1A24901-1E5F-4C1C-B0CD-16A631AE9CBD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569263.5002 5935374.7943 569262.8387 5935346.3515 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_250131BF-2D0D-46D5-A071-A673A1065442" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569262.8387 5935346.3515 569253.5783 5935290.1274 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DBF55D20-8384-402B-897E-5A6AD330D9B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569253.5783 5935290.1274 569230.4272 5935201.4918 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_25BD8C8F-ABEA-4729-A816-00563BF97FFA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569230.4272 5935201.4918 569342.2139 5935217.3669 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FF087563-4C1B-4BEE-BED4-14EA1B167B26" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569342.2139 5935217.3669 569395.1307 5935214.721 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B608CD39-C8E2-4244-9EDF-9539FBA443A7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569395.1307 5935214.721 569386.5317 5935188.9241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6FC22DAE-118A-46DF-9D65-D1F6BD52AC81" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569386.5317 5935188.9241 569367.3494 5934915.0798 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7829B2E6-0ABE-45BF-A50F-EF261FF0AEEF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569367.3494 5934915.0798 569313.1097 5934876.7151 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C4DA967C-759B-4B38-9AAF-98330ED3FBDB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569313.1097 5934876.7151 569313.1097 5934665.7095 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_56A2BD6B-7DD5-4CD0-9F11-2E66C1A4ECA0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569313.1097 5934665.7095 569323.693 5934653.1418 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B2774F3D-0029-48A6-ABF6-4CEACF6815E6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569323.693 5934653.1418 569340.891 5934658.4334 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_13D37652-6E5C-41E0-A8E1-D7ABD82768EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569340.891 5934658.4334 569365.365 5934666.371 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FB8CF6A2-8B52-41A7-BD55-B79DA24C90FA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569365.365 5934666.371 569391.1619 5934668.3553 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D24D49A9-9770-41E8-BC74-4AFB87E19C59" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569391.1619 5934668.3553 569423.5734 5934665.7095 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8F592225-0C99-4DEB-B6DC-FDB46FC56125" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569423.5734 5934665.7095 569508.2403 5934659.7564 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AB9D91ED-B591-4DB2-AA2A-C89B40B1947F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569508.2403 5934659.7564 569560.4956 5934662.4022 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_080AFDE1-E2B4-450F-99B0-52C41FFCA19B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569560.4956 5934662.4022 569604.8134 5934672.9856 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_41EE0ED5-6882-4B1D-B9D1-5584FB4AA4C2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569604.8134 5934672.9856 569609.5671 5934663.4148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_E60D83C7-8C52-4403-9190-69AD9B871EB6" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569609.5671 5934663.4148 569610.172928846 5934663.65695647 569610.778 5934663.901 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2E49EDBB-C71A-404C-B037-EBCCC3A3C398" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569610.778 5934663.901 569620.497 5934642.516 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A6E2BBAB-83A1-4BD4-AE54-C9FEE37A0FB2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569620.497 5934642.516 569632.983 5934643.015 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_00E82C41-96EC-49AC-9E4B-B5DC9D864868" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569632.983 5934643.015 569652.261 5934648.647 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B3BB6594-1C02-475D-8551-CE04A3060FBD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569652.261 5934648.647 569678.196 5934656.226 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5432C2B1-A7AA-4138-B184-372C88CCB875" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569678.196 5934656.226 569689.513 5934659.534 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B9E71C90-E187-4B5C-A26D-F6B8F4F285B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569689.513 5934659.534 569737.139 5934664.504 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_20DA93CF-F837-4413-8DD8-FA3CF653E824" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569737.139 5934664.504 569757.65 5934664.844 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_51AF017D-9134-4C8C-ADE5-96B9279D5107" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569757.65 5934664.844 569761.984 5934653.458 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F1341F2D-B4E5-41E6-A10B-A11A812F058E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569761.984 5934653.458 569767.540203225 5934633.48048933 569777.23 5934615.148 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_DF18C012-FA92-49B5-8159-8BDEEC5C4DCA" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569777.23 5934615.148 569778.796034083 5934613.03609136 569780.565 5934611.091 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_21B52819-61F0-443E-991F-AE8336BCDCE5" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569780.565 5934611.091 569789.544642533 5934603.32786963 569799.2186 5934596.4493 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4BDF4963-B9A1-4CFE-B3B0-F31000CCD304" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569799.2186 5934596.4493 569801.2669 5934594.9333 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_630CE2F3-F910-4D36-A5A9-7969E14D0010" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569801.2669 5934594.9333 569874.6889 5934548.6311 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2EFD97C-9EE1-4A4C-B25B-1BB34A647787" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569874.6889 5934548.6311 570012.2725 5934524.8186 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FEED119C-2029-4F12-A041-BD2D9DB0EC00" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570012.2725 5934524.8186 570099.5852 5934520.1884 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E2D60B17-6873-461A-8D93-E8732BE4B9A3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570099.5852 5934520.1884 570301.9918 5934487.7769 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_402D60B2-5ED7-4C44-9218-3ACF5E0CC62B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570301.9918 5934487.7769 570473.3099 5934446.1049 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9A12A49D-29C3-4AF6-AF06-32A381412BEA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570473.3099 5934446.1049 570469.296 5934431.583 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E07744A2-CA2F-4AF7-9283-752EBA1A7DC3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570469.296 5934431.583 570455.282 5934397.443 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C23AF1D6-E853-477A-87CE-AFC3961AFD3C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570455.282 5934397.443 570452.496 5934391.2454 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_385CA8D9-BDEB-40CC-9B32-29289D57431F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570452.496 5934391.2454 570450.61 5934387.05 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7F7C88F8-6BA2-4AEA-990C-38DEE6441085" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570450.61 5934387.05 570451.053 5934386.865 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E9CEE15F-1871-456E-822C-A6A951E54563" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570451.053 5934386.865 570448.733 5934381.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_11196997-D479-4CD7-95AD-4C1DAFD89A02" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570448.733 5934381.725 570441.101 5934365.283 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F39633BA-BD43-4EE8-ACB9-0ADFC52388CB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570441.101 5934365.283 570454.186 5934362.003 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2F6CF4B8-110A-4B23-8AD0-D83A289414AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570454.186 5934362.003 570461.075 5934360.277 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_95806C38-1ADD-4FFC-909D-B371690213B0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570461.075 5934360.277 570448.937 5934334.012 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_45B06887-4B83-4783-9164-66B7E50154AC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570448.937 5934334.012 570433.249 5934337.943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E9176D5D-E297-4025-8CD0-3E0DBED5FEC6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570433.249 5934337.943 570416.681 5934296.146 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:externeReferenz>
        <xplan:XP_SpezExterneReferenz>
          <xplan:art>Dokument</xplan:art>
          <xplan:referenzName>StErhVO Hamm</xplan:referenzName>
          <xplan:referenzURL>http://test.de/StErhVO_Hamm.pdf</xplan:referenzURL>
          <xplan:typ>3100</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:externeReferenz>
      <xplan:texte xlink:href="#Gml_85EBE485-0973-4AEB-8DD0-5E7757E9421E"/>
      <xplan:texte xlink:href="#Gml_A39B1B6A-8BA3-4774-A7B7-B50CB95ADFA9"/>
      <xplan:texte xlink:href="#Gml_57A82595-4F7C-43D8-A492-20E8D50E0EC5"/>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>020000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>122, 123, 125</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt codeSpace="www.mysynergis.com/XPlanungR/5.2/0">17200</xplan:planArt>
      <xplan:bereich xlink:href="#Gml_3A57FCDE-C5EC-44BA-99B9-70DE8F95D543"/>
    </xplan:SO_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_85EBE485-0973-4AEB-8DD0-5E7757E9421E">
      <xplan:schluessel>1</xplan:schluessel>
      <xplan:text>Diese Verordnung gilt für das in der anliegenden Karte durch eine schwarze Linie umgrenzte Gebiet südlich der Gleisanlage parallel zur Marienthaler Straße, zwischen Gütertransportgleisanlage und Perthesweg, Mettlerkampsweg, Morahtstieg und Schurzallee-Nord, entlang Eiffestraße, Rückersweg bis Rückersbrücke, entlang Mittelkanal bis Erste Borstelmannbrücke, entlang Borstelmannsweg, Eiffestraße, Osterbrook, Dobbelersweg, Hübbesweg, Droopweg bis zur östlichen Kante des Flurstücks 149, über die östliche Kante des Flurstücks 1418 bis Mitte Hammer Landstraße, entlang Hammer Landstraße, Hammer Steindamm, entlang nördlich der Gleisanlage, entlang Hirtenstraße, entlang Meridianstraße, Sievekingdamm, Saling, Sievekingsallee, Ritterstraße, parallel Marienthaler Straße in den Ortsteilen 122, 123, 125 der Gemarkungen Hamm-Geest und Hamm-Marsch im Bezirk Hamburg- Mitte.
Das Gebiet wird wie folgt begrenzt:
Ab Mitte Peterskampweg nach Norden bis zum Bahnkörper, nach Osten entlang der nördlichen Grenzen der Flurstück 850,
852, 651 und 79, über Hammer Steindamm, Flurstücke 1125, 231, 192, 193 und 194, über Marienthaler Straße, Flurstücke
1899 und 1904, der westlichen Grenze des Flurstücks 1904 bis zur östlichen Stadtteilgrenze Hamm, dieser nach Süden bis
Sievekingsallee, über Sievekingsallee bis Horner Weg, über Horner Weg bis Hammer Landstraße, über Hammer Landstraße
bis zur Straßenmitte Schurzallee-Nord weiter bis Eiffestraße, über Eiffestraße weiter entlang Rückersweg nach Süden
bis Mittelkanal, kanalmittig weiter nach Westen bis Mitte Erste Borstelmannbrücke, weiter nach Norden über Mitte
Borstelmannsweg bis Mitte Eiffestraße, dieser nach Osten bis Mitte Osterbrook, diesem nach Norden über Diagonalstraße
bis Mitte Dobbelersweg, diesem nach Osten bis Mitte Hübbesweg, diesem nach Norden bis Mitte Droopweg, diesem nach
Osten bis zur östlichen Kante des Flurstücks 149, deren Verlauf nach Norden über die östliche Kante des Flurstücks 1418
bis zur Mitte Hammer Landstraße, dieser nach Westen bis zur nördlichen Flurstücksgrenze 1685 (Thörls Park) deren Verlauf
bis zur Mitte Hirtenstraße, diesem nach Westen bis Mitte Meridianstraße, dieser nach Norden bis zur Mitte Sievekingdamm,
diesem nach Nordosten bis zur Mitte Saling, dieser nach Norden bis zur Mitte Sievekingsallee, dieser nach Westen bis zur Mitte Ritterstraße, dieser nach Norden bis zur Mitte Marienthaler Straße, dieser nach Osten bis zur westlichen Flurstücksgrenze 862, dieser nach Norden bis zur Grenze der Bahntrasse, entlang der hinteren Grenzen der Flurstücke 862 und 812, entlang der östlichen Flurstücksgrenze 812 nach Süden bis zur Mitte Marienthaler Straße, dieser nach Osten bis zum Ausgangspunkt Mitte Peterkampsweg folgend.</xplan:text>
      <xplan:rechtscharakter>9999</xplan:rechtscharakter>      
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_A39B1B6A-8BA3-4774-A7B7-B50CB95ADFA9">
      <xplan:schluessel>2</xplan:schluessel>
      <xplan:text>Zur Erhaltung der städtebaulichen Eigenart des Gebietes auf Grund seiner städtebaulichen Gestalt bedürfen in dem
in Absatz 1 bezeichneten Gebiet der Rückbau, die Änderung, die Nutzungsänderung sowie die Errichtung baulicher Anlagen
der Genehmigung, und zwar auch dann, wenn nach den bauordnungsrechtlichen Vorschriften eine Genehmigung
nicht erforderlich ist. Die Genehmigung zum Rückbau, zur Änderung oder zur Nutzungsänderung darf nur versagt werden,
wenn die bauliche Anlage allein oder im Zusammenhang mit anderen baulichen Anlagen das Ortsbild, die Stadtgestalt
oder das Landschaftsbild prägt oder sonst von städtebaulicher, insbesondere geschichtlicher oder künstlerischer Bedeutung
ist. Die Genehmigung zur Errichtung der baulichen Anlage darf nur versagt werden, wenn die städtebauliche Gestalt des
Gebiets durch die beabsichtigte bauliche Anlage beeinträchtigt wird.</xplan:text>
      <xplan:rechtscharakter>9999</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_57A82595-4F7C-43D8-A492-20E8D50E0EC5">
      <xplan:schluessel>3</xplan:schluessel>
      <xplan:text>Es wird auf Folgendes hingewiesen:
Unbeachtlich werden
a) eine nach § 214 Absatz 1 Satz 1 des Baugesetzbuchs beachtliche Verletzung der dort bezeichneten Verfahrens- und
Formvorschriften und
b) nach § 214 Absatz 3 Satz 2 des Baugesetzbuchs beachtliche Mängel des Abwägungsvorgangs,
wenn sie nicht innerhalb eines Jahres seit Bekanntmachung dieser Verordnung schriftlich gegenüber dem örtlich zuständigen
Bezirksamt unter Darlegung des die Verletzung begründenden Sachverhalts geltend gemacht worden sind.</xplan:text>
      <xplan:rechtscharakter>9999</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Bereich gml:id="Gml_3A57FCDE-C5EC-44BA-99B9-70DE8F95D543">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>569230.4272 5933929.5049</gml:lowerCorner>
          <gml:upperCorner>570727.964 5935766.084</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:name>defaultBereich</xplan:name>
      <xplan:bedeutung>9999</xplan:bedeutung>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_9AE5D976-829C-4019-8A1F-DEC180817FBA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D0E6E56-5198-406B-B7C2-0391E999EC7B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570416.681 5934296.146 570413.1171 5934290.6619 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9144B6F9-B122-41DA-B2B2-D9AF5F91EB8E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570413.1171 5934290.6619 570227.2469 5934323.7349 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_12722611-B657-4CED-A871-8FA00D0C570E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570227.2469 5934323.7349 570173.0072 5934215.2555 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CCE26DCA-82D5-4DC9-B8F0-5161DEE546A9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570173.0072 5934215.2555 570127.3665 5934223.8544 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0044D12E-1CFF-465D-94B5-C51894F4F1AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570127.3665 5934223.8544 570113.063 5934192.394 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_38932181-87E2-418E-929F-3D6CD82DB044" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570113.063 5934192.394 570073.533 5934112.959 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_24EFEF5C-2C9F-4EB8-8FC5-166EC5D9B766" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570073.533 5934112.959 570067.1737 5934098.8386 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_69F3596C-E983-433D-9DDE-C115635AD0D0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570067.1737 5934098.8386 569824.418 5934145.1408 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A079DBA2-7861-43D6-A04F-F7596ACE51BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569824.418 5934145.1408 569782.746 5934058.4895 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1D73AAD9-2B93-4880-9590-7F2A2079B44A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569782.746 5934058.4895 570495.7995 5933929.5049 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ED380864-ACAA-4E83-AACD-594C97821EFA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570495.7995 5933929.5049 570553.3465 5934012.1874 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_281E1236-C8F0-4788-9F31-40E8A590E1D0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570553.3465 5934012.1874 570689.6072 5933993.005 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D0495DFD-AC3C-43A5-96E4-BEC5D79803D2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570689.6072 5933993.005 570710.7739 5934180.1981 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4FB3F4F0-8911-4832-AC65-02B515D4D311" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570710.7739 5934180.1981 570715.4041 5934237.7451 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B829A738-13BD-4F1C-9B5A-BF2B1513C3DE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570715.4041 5934237.7451 570719.3729 5934252.9587 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D8A31209-3335-4BF2-81F7-BF854BD1DB49" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570719.3729 5934252.9587 570708.1281 5934352.1776 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3FF5674C-B717-4228-B3CD-D2FF8FB88992" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570708.1281 5934352.1776 570727.964 5934394.294 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65F5F745-6131-4893-9412-6E3956B1F132" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570727.964 5934394.294 570725.183 5934419.353 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9E52AA0A-46A5-4C21-9E6A-C725165E1950" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570725.183 5934419.353 570722.749 5934419.242 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65E69A55-42F0-46B1-8884-B2B38EE5AD91" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570722.749 5934419.242 570722.436 5934421.973 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_938F09A2-4D24-4EDA-AF96-0098F9BE2C74" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570722.436 5934421.973 570720.021 5934443.076 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_10F05380-6622-4B86-9E89-EF1FDB08A547" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570720.021 5934443.076 570719.593 5934446.813 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F5B7F72B-33C4-4A17-A1C8-5090A20C51BD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570719.593 5934446.813 570717.285 5934449.018 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E543FB77-ED38-44AE-9C67-A972AA2F5F98" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570717.285 5934449.018 570709.556 5934507.593 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D039F495-698A-411E-B019-8BCB957E09E6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570709.556 5934507.593 570699.09 5934586.865 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_77892168-A29B-4FEA-ACCC-753687ED8D53" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570699.09 5934586.865 570696.699 5934606.715 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2DEE8E3D-A2CA-41CE-B6F2-EB5467F35E4A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570696.699 5934606.715 570682.5221 5934724.3021 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_64FF70CA-9918-4363-9E95-A8DBC4314E06" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570682.5221 5934724.3021 570663.668 5934880.6839 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3E859638-A671-4D41-A7D3-A83B084DC085" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570663.668 5934880.6839 570652.427 5934973.92 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5B36640F-3815-4ADE-A376-0D5008E93A7E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570652.427 5934973.92 570651.9607 5934977.4523 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0C6E6FD3-6E92-4DB8-BB4E-CC52BBCC674D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570651.9607 5934977.4523 570647.911 5935008.13 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_46094E05-BE9F-4C20-ADCA-DEA09A46EA0E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570647.911 5935008.13 570644.512 5935023.756 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F3D26511-64AC-4F24-8E24-78C8731B413F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570644.512 5935023.756 570631.979 5935045.949 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40352C02-0423-4792-8272-901106DB907F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570631.979 5935045.949 570617.25 5935084.336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B309FD3A-4957-47A3-B307-E0F1AE38E263" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570617.25 5935084.336 570610.8745 5935100.95 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_57DC946A-9F2F-4D21-8732-3966376D2908" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570610.8745 5935100.95 570501.699 5935385.449 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C12CB1E3-AC15-4B51-95A0-81477AF17E8B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570501.699 5935385.449 570473.004 5935460.231 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A59FA43D-D5DD-47B0-A2C0-3156E2D4829D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570473.004 5935460.231 570398.174 5935648.743 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FAC0FFBA-3FFF-4A69-8C36-BE6D76AFCB54" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570398.174 5935648.743 570389.877 5935669.644 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FD2362F9-2379-4120-A1AD-1B8DB0229DC4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570389.877 5935669.644 570352.505 5935763.753 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CA999B0C-092C-41BC-A0AA-D255F473F5FE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570352.505 5935763.753 570351.579 5935766.084 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_495E4BB1-3FAA-45F4-A93C-1C14E03C0B80" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570351.579 5935766.084 570305.964 5935743.357 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_349BFEEA-3297-40E0-A3CB-75DB97BBCB41" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570305.964 5935743.357 570241.835 5935711.403 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_971C212F-BD84-4AB9-B0B0-CE9BC311A1DD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570241.835 5935711.403 570240.99 5935717.756 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FFB55FB6-A006-456F-8557-5A80B3C5C4F0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570240.99 5935717.756 570224.815 5935712.291 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EFA78087-49E5-4ED7-9C9E-DA2911BE477B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570224.815 5935712.291 570183.682 5935693.091 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C3F2C350-5410-41C8-A889-C1C3D96BEDBD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570183.682 5935693.091 570145.609 5935677.461 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B0C90972-C079-44CE-88A2-E9B27781CED5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570145.609 5935677.461 570111.089 5935663.444 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A0A6E839-DB10-4DCE-A665-6ACBACDE5710" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570111.089 5935663.444 570111.248 5935661.853 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D069D6CE-41E4-4B4E-9AAC-0D89E9C3011D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570111.248 5935661.853 570062.741 5935640.65 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8BBAE38E-C8FA-4969-B86F-7E7AE4C840CB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570062.741 5935640.65 570047.866 5935634.148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9D3FA5E3-2217-4BE9-A82B-6767A6436646" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570047.866 5935634.148 570047.439 5935637.031 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B7D5802-E7BF-4870-B545-5816B87FDBC8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570047.439 5935637.031 570043.259 5935635.114 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_178C934F-F591-4397-93B5-3CBA9CDE5648" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570043.259 5935635.114 570042.825 5935638.214 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AAF39036-0C16-459B-9CA3-7F46B1AE9775" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570042.825 5935638.214 570040.0538 5935636.943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_10AE7DA0-1BCF-497D-9F42-AC93866E7D39" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570040.0538 5935636.943 570028.402 5935631.599 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F8805660-280E-4016-9EC3-A3EA9674F8DD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570028.402 5935631.599 570022.267 5935628.786 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6AEA1479-73B5-4F73-A98B-FE5B1462FCF2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570022.267 5935628.786 570021.398 5935620.08 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BF783AE7-AB95-4D1B-ADF1-F8B035E409EF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570021.398 5935620.08 570019.966 5935619.369 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_676C347E-A7F3-4D48-B089-4C125BC54E80" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570019.966 5935619.369 570015.343 5935617.078 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC94FBD3-3C63-45B0-A3C8-A3CB2C00150D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570015.343 5935617.078 570005.198 5935612.047 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3CD1C2A2-895A-43F7-96A2-EE5E2A455501" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570005.198 5935612.047 569987.794 5935604.094 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D7B17C6E-599D-43D9-94E7-9351C59037B8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569987.794 5935604.094 569978.59 5935599.889 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C4E3DCD6-C825-4A6E-B349-A5A7F100420F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569978.59 5935599.889 569964.257 5935593.341 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EF15ED5B-7948-4ACB-BBAE-C668C9235234" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569964.257 5935593.341 569957.926 5935590.835 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9B82F742-F407-46B2-981B-96D7B36750C6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569957.926 5935590.835 569927.016 5935578.592 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EAA49108-C4EF-46CD-B362-0FD891126852" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569927.016 5935578.592 569898.702 5935567.541 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8199294F-752B-454E-8E49-887BACB14F1E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569898.702 5935567.541 569889.65 5935564.007 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_489470FB-C0E4-4289-8D47-F1E8C8CDC998" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569889.65 5935564.007 569852.189 5935549.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3A625BF3-A16A-4723-8267-099E09594837" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569852.189 5935549.725 569815.032 5935534.788 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_206E32B6-B4F9-41C4-99F4-9DD5B826736C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569815.032 5935534.788 569809.8659 5935533.0967 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_18E4221C-61A0-44BD-84E4-3CC99DAB0AFA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569809.8659 5935533.0967 569802.487 5935530.681 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B6A4376B-3D15-4B6B-B3C0-593A1896CBDE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569802.487 5935530.681 569782.3991 5935524.1047 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1F1DD015-E1B3-479A-AC52-778F81171048" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569782.3991 5935524.1047 569757.725 5935516.027 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42F3EA75-29FD-4CF8-B5BD-4808261BE7C9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569757.725 5935516.027 569707.595 5935502.147 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_587B59D8-385E-4ACE-B990-B8137A55E154" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569707.595 5935502.147 569682.885 5935497.12 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_05945FAC-DB4A-453F-AC0B-3A4E454CDD5E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569682.885 5935497.12 569674.348 5935495.002 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C5B1A460-DD93-41C3-A475-AADD3005B5B5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569674.348 5935495.002 569654.498 5935490.799 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D6261A37-E882-477B-916B-699B879EBA52" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569654.498 5935490.799 569635.411 5935487.319 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4E5E0755-877D-4078-8F03-AF58C54CD4A5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569635.411 5935487.319 569635.2405 5935487.2883 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CF300667-C75C-4F9A-BB55-24F4B58A1007" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569635.2405 5935487.2883 569625.5071 5935485.5347 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_945803C1-B203-49F8-BB20-781F9EBE48A2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569625.5071 5935485.5347 569625.3186 5935485.9195 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B0AF2377-8208-4C9A-8CC6-6BC84195A66A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569625.3186 5935485.9195 569616.7196 5935425.7266 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B4E47581-F975-40EA-A4EE-A05F7AD37FF6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569616.7196 5935425.7266 569422.912 5935401.9141 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_024BC847-4E94-4A99-9CE4-75B8DD9F47F0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569422.912 5935401.9141 569416.528 5935460.741 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CF76C259-3393-4941-8910-32E3097C2B99" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569416.528 5935460.741 569383.741 5935457.115 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7DFF954A-BC1A-44C5-AA94-29048D836F6F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569383.741 5935457.115 569391.8234 5935395.961 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3376E95B-EA04-440E-BB46-D1A33EC05F42" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569391.8234 5935395.961 569337.5837 5935389.3464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_55A3A355-1E8A-41D8-BD11-796A36858DE2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569337.5837 5935389.3464 569263.5002 5935374.7943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EB66EE7B-1F4B-4234-B237-09F8B7EA907D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569263.5002 5935374.7943 569262.8387 5935346.3515 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8311D13A-C6B1-44FA-9E8D-A047A7F8B139" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569262.8387 5935346.3515 569253.5783 5935290.1274 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_74CFEC2E-6B4F-4C63-96AD-330052C2AEE7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569253.5783 5935290.1274 569230.4272 5935201.4918 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5B580620-CD0D-49B7-8A3C-1DBA8728E845" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569230.4272 5935201.4918 569342.2139 5935217.3669 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9B60E9A3-96E4-4B81-96CC-04D492A93712" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569342.2139 5935217.3669 569395.1307 5935214.721 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7D2A9272-07DE-4F18-97A5-5A82E3749DC6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569395.1307 5935214.721 569386.5317 5935188.9241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C6F0B112-7816-4A42-9205-0DBE902F53EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569386.5317 5935188.9241 569367.3494 5934915.0798 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D8E65B28-B71C-4F1F-BAD7-5F5131ACC1AD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569367.3494 5934915.0798 569313.1097 5934876.7151 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C569BA67-9ECA-4526-8205-DB87A2B170C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569313.1097 5934876.7151 569313.1097 5934665.7095 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AAF26B5B-9516-4B7F-86DB-E1DA9E43F95F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569313.1097 5934665.7095 569323.693 5934653.1418 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A5E09D85-17E3-4D76-AF58-8BACD36EE00E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569323.693 5934653.1418 569340.891 5934658.4334 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F86B63E5-DF39-4C7A-9127-36F6398B092C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569340.891 5934658.4334 569365.365 5934666.371 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_114F082D-8F06-45A7-A510-60CA1756A877" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569365.365 5934666.371 569391.1619 5934668.3553 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_15DCB55F-ADEF-4198-B6E6-3549B776817E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569391.1619 5934668.3553 569423.5734 5934665.7095 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E37F99B4-87D1-4547-BB1C-69892EAF0ACE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569423.5734 5934665.7095 569508.2403 5934659.7564 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0550828C-977A-4882-B54F-25AA6312D230" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569508.2403 5934659.7564 569560.4956 5934662.4022 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0D59A955-A874-4FEF-A5E9-AA3C8F5ABBF1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569560.4956 5934662.4022 569604.8134 5934672.9856 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_300D3865-20F0-479A-8134-3A6ABBBC0D6E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569604.8134 5934672.9856 569609.5671 5934663.4148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_E06DECA0-66D0-4B35-B8FE-469D013A467F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569609.5671 5934663.4148 569610.172928846 5934663.65695647 569610.778 5934663.901 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7012707C-596B-4EAD-A8A9-F247CD08FA65" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569610.778 5934663.901 569620.497 5934642.516 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BF678F56-3448-49D6-BB5A-E4C8A9BBDC12" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569620.497 5934642.516 569632.983 5934643.015 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E8BC7F2-B106-4C5A-9661-8D437C8C0941" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569632.983 5934643.015 569652.261 5934648.647 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7733D203-AF0C-4F9D-9994-E3E59FAEB54A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569652.261 5934648.647 569678.196 5934656.226 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E6A24651-18D3-4355-B45B-162FA4A9DEB2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569678.196 5934656.226 569689.513 5934659.534 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1AFDDA93-627F-44C6-A909-30C9736216D8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569689.513 5934659.534 569737.139 5934664.504 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_73CD1183-1F5A-4A34-8397-31057F5C8C48" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569737.139 5934664.504 569757.65 5934664.844 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9FC072AF-10C3-4086-88EC-FE8E26BE9C0D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569757.65 5934664.844 569761.984 5934653.458 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_4215B2A6-2C71-4AEB-9574-6C560C8C7AB5" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569761.984 5934653.458 569767.540203225 5934633.48048933 569777.23 5934615.148 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_89E38EC1-A795-4908-BA29-F25B65E6501B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569777.23 5934615.148 569778.796034083 5934613.03609136 569780.565 5934611.091 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_104994C1-5AFB-4802-A8CB-3623CABF22C1" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569780.565 5934611.091 569789.544642533 5934603.32786963 569799.2186 5934596.4493 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C7BD996F-B79B-477D-B147-1BF1D498994E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569799.2186 5934596.4493 569801.2669 5934594.9333 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_060D2056-733E-4101-BB37-9665A32FD0F7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569801.2669 5934594.9333 569874.6889 5934548.6311 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_971C3670-1528-427D-A445-E37AD4199AC6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569874.6889 5934548.6311 570012.2725 5934524.8186 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_41EB8434-ADAE-4154-A6E0-D109F7A07EC3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570012.2725 5934524.8186 570099.5852 5934520.1884 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_67E86143-C153-436E-8A57-A0478F70D7DE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570099.5852 5934520.1884 570301.9918 5934487.7769 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0A9D7633-331E-4A86-AE94-567B724CDF24" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570301.9918 5934487.7769 570473.3099 5934446.1049 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D097AD8-11D9-4A0C-A30A-B58C6FCC70C6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570473.3099 5934446.1049 570469.296 5934431.583 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9B3E3703-C427-4DB8-A0C3-9124BC9D0DAF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570469.296 5934431.583 570455.282 5934397.443 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CC227195-B811-42F5-AC6D-C9A84A312A4C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570455.282 5934397.443 570452.496 5934391.2454 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_466ACB5C-F062-410F-A7A5-9799158ED3B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570452.496 5934391.2454 570450.61 5934387.05 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3769DF64-4FB9-4478-984F-446CFB0FAE68" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570450.61 5934387.05 570451.053 5934386.865 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_70263C19-022B-4230-B1CE-CCFBEAA31D82" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570451.053 5934386.865 570448.733 5934381.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1854FAB6-A614-4708-AA56-BF205F528ECB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570448.733 5934381.725 570441.101 5934365.283 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_98379304-2340-4455-AE78-0EFFD0258971" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570441.101 5934365.283 570454.186 5934362.003 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_01A368BE-7157-43F3-9043-5162EA3A8F5C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570454.186 5934362.003 570461.075 5934360.277 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F4EB0F08-24A0-4BDA-B1B1-A75BF11FBD9C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570461.075 5934360.277 570448.937 5934334.012 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B73C252-CDE8-40B2-8971-4B90882726B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570448.937 5934334.012 570433.249 5934337.943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_00120215-25B7-4094-858D-6E602AA55090" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570433.249 5934337.943 570416.681 5934296.146 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_28DCCD04-EE15-494D-8711-601B682BDBFB"/>
      <xplan:gehoertZuPlan xlink:href="#Gml_1A8DF387-B40C-4E65-9B58-2C2BC092249C"/>
    </xplan:SO_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gebiet gml:id="Gml_28DCCD04-EE15-494D-8711-601B682BDBFB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>569230.4272 5933929.5049</gml:lowerCorner>
          <gml:upperCorner>570727.964 5935766.084</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_3A57FCDE-C5EC-44BA-99B9-70DE8F95D543"/>
      <xplan:rechtscharakter>9999</xplan:rechtscharakter>
      <xplan:sonstRechtscharakter codeSpace="www.mysynergis.com/XPlanungR/5.2/0">Verordnung</xplan:sonstRechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_84CFF066-DEE7-4B42-9545-52D465B3C42A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3E87D103-96C5-4879-A28C-9FBEB908277B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570416.681 5934296.146 570413.1171 5934290.6619 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_813AB913-54BB-4E83-A06F-9957A3C512C9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570413.1171 5934290.6619 570227.2469 5934323.7349 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1569D45C-50BF-4594-893A-8E7487F8FDD1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570227.2469 5934323.7349 570173.0072 5934215.2555 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_238F7990-15B5-41C7-8512-C7D9476768D0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570173.0072 5934215.2555 570127.3665 5934223.8544 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E4128B72-F80B-46FB-B983-6C9AA70045E2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570127.3665 5934223.8544 570113.063 5934192.394 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CF0EDA4D-085B-4D5A-9F1F-8586D6D13A7A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570113.063 5934192.394 570073.533 5934112.959 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F8644765-88CC-4A59-A06D-CA65F5C96CEA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570073.533 5934112.959 570067.1737 5934098.8386 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A33D84EF-42AA-4620-A9BA-FD1B6233BE1C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570067.1737 5934098.8386 569824.418 5934145.1408 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_82D73F19-89C1-4298-8B02-72C5E8949AB8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569824.418 5934145.1408 569782.746 5934058.4895 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2743B123-4378-4CF6-BE97-67BEEEAC23F1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569782.746 5934058.4895 570495.7995 5933929.5049 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_52F3A731-3038-4EDA-B99F-3F44C485B888" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570495.7995 5933929.5049 570553.3465 5934012.1874 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2AFE83B8-2113-4893-AEDA-BF982BB30EBE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570553.3465 5934012.1874 570689.6072 5933993.005 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FE12808F-2EFD-4D95-B4B5-AB9FD41DF3BA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570689.6072 5933993.005 570710.7739 5934180.1981 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_74CEF9ED-E115-4B4F-B837-01719CF3FAFB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570710.7739 5934180.1981 570715.4041 5934237.7451 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9FFA5C9D-F7B3-43A8-9E56-84CF7BC42568" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570715.4041 5934237.7451 570719.3729 5934252.9587 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F2172A5D-E7DF-4A61-8CBA-1F44F9D51AF0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570719.3729 5934252.9587 570708.1281 5934352.1776 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8E449E27-168F-4AD2-8B08-308428A48A52" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570708.1281 5934352.1776 570727.964 5934394.294 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_704162CC-94E8-4841-BC80-5C71167123A2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570727.964 5934394.294 570725.183 5934419.353 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0D447022-1AA8-47CB-8BE8-4D042BD8F244" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570725.183 5934419.353 570722.749 5934419.242 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_37410E70-6CA7-4E78-9917-9B8D5A1351C8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570722.749 5934419.242 570722.436 5934421.973 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_79C68495-9703-45CD-AEE5-1B69118280C7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570722.436 5934421.973 570720.021 5934443.076 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_45F53947-66EB-4955-A9DC-A558AD111BBF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570720.021 5934443.076 570719.593 5934446.813 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1107D8B7-0B6F-454C-B39D-B7CE5D8120A0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570719.593 5934446.813 570717.285 5934449.018 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_741DC3DF-93D8-4F00-8EF8-E5BCCF32B595" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570717.285 5934449.018 570709.556 5934507.593 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3872B02D-CE2F-4DD5-9263-48D46B676BA6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570709.556 5934507.593 570699.09 5934586.865 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0CE3CE93-8996-4EDC-91EA-330D4F277B60" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570699.09 5934586.865 570696.699 5934606.715 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_08D8569C-CDA2-4471-8275-CB7F5CABD838" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570696.699 5934606.715 570682.5221 5934724.3021 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D2956347-33AE-435D-AFAF-E154C065F7E3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570682.5221 5934724.3021 570663.668 5934880.6839 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42470629-D14E-497D-B7E5-AFA4743CD3F4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570663.668 5934880.6839 570652.427 5934973.92 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7854AD99-D0A9-4933-8925-F8C761B77F94" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570652.427 5934973.92 570651.9607 5934977.4523 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E629908-5BA1-416F-9A19-798275643575" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570651.9607 5934977.4523 570647.911 5935008.13 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_587B3864-56D8-47EF-B017-8BAE78D3F276" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570647.911 5935008.13 570644.512 5935023.756 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_09E7FCC9-CB44-462B-97FC-4D9D6730B066" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570644.512 5935023.756 570631.979 5935045.949 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_10213958-FBCE-4A6A-89AD-18E9BD557973" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570631.979 5935045.949 570617.25 5935084.336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_48019AEA-00CE-4D97-B51F-90CAF682C997" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570617.25 5935084.336 570610.8745 5935100.95 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_955A4735-07B7-49C4-97D2-0DC14F100A2F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570610.8745 5935100.95 570501.699 5935385.449 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8AE7C9C1-68CC-4755-85C9-0C8334354253" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570501.699 5935385.449 570473.004 5935460.231 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2A2B0821-51C6-4580-A361-F31A4C375298" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570473.004 5935460.231 570398.174 5935648.743 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_82BBB6A3-FE74-49FD-A882-9CFF326A72AF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570398.174 5935648.743 570389.877 5935669.644 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_286FD31B-2F2E-44E0-8709-1FA121B72537" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570389.877 5935669.644 570352.505 5935763.753 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8E033C76-51E4-41D2-B78C-73EC84F2DD74" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570352.505 5935763.753 570351.579 5935766.084 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C8046DDA-659D-42EE-AA0E-DD2BCFB885CC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570351.579 5935766.084 570305.964 5935743.357 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A3F4862C-464D-4B34-AB6F-1CA4143D0E2F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570305.964 5935743.357 570241.835 5935711.403 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C4A2418E-E4E1-446B-B036-235F757596E7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570241.835 5935711.403 570240.99 5935717.756 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_950CD072-6F22-45F7-BD84-273C0794A2FF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570240.99 5935717.756 570224.815 5935712.291 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_89A5C395-8479-4B59-A89D-1C9C362ED15F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570224.815 5935712.291 570183.682 5935693.091 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_46AE73AF-82D4-4D4B-8D91-C1FB2233CD42" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570183.682 5935693.091 570145.609 5935677.461 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7001C5FA-8360-45C6-BFB9-A96B84E78011" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570145.609 5935677.461 570111.089 5935663.444 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0384DE69-0F03-4147-A5C2-E8C88FCA196B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570111.089 5935663.444 570111.248 5935661.853 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_61BE186D-DD2D-448E-9B29-8E8090F074E0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570111.248 5935661.853 570062.741 5935640.65 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2B049267-02AF-4EE4-A3AE-0589C4B968B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570062.741 5935640.65 570047.866 5935634.148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_47AE4886-4B61-405C-93DE-69FBDBFE8EBB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570047.866 5935634.148 570047.439 5935637.031 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65BF6992-C0F3-4B15-A9AB-D09AAA3684EB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570047.439 5935637.031 570043.259 5935635.114 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_72590021-72B5-4E41-8BAE-82CADEC24D51" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570043.259 5935635.114 570042.825 5935638.214 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EDF700DE-B9B2-40F7-B4E6-BAAC9DA69AD8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570042.825 5935638.214 570040.0538 5935636.943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B35FCF20-7957-4D06-BEC2-F570FD462C0D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570040.0538 5935636.943 570028.402 5935631.599 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_040818AC-C9A6-4155-834E-F02DD4839C52" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570028.402 5935631.599 570022.267 5935628.786 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6DFFC430-9621-4B70-9E0D-E11AD57650C6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570022.267 5935628.786 570021.398 5935620.08 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_741A46CF-C2EA-4799-AC2A-9AE9F1B6B844" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570021.398 5935620.08 570019.966 5935619.369 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_55A705C6-9150-44F3-A1E8-32F0AB4201DA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570019.966 5935619.369 570015.343 5935617.078 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BD0794B9-A725-453E-B89B-7ED2FCB96295" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570015.343 5935617.078 570005.198 5935612.047 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F962F097-DB86-4988-8541-005A7462525C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570005.198 5935612.047 569987.794 5935604.094 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A8E5E566-2CD2-4E59-A134-7469276AE28C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569987.794 5935604.094 569978.59 5935599.889 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_11113CFA-3251-4549-B049-B97021F54326" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569978.59 5935599.889 569964.257 5935593.341 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_59C38CD0-C735-4ABD-A4DA-BD6D441486A9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569964.257 5935593.341 569957.926 5935590.835 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7C738A8B-56D6-4EBB-9509-E7DD330B2A24" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569957.926 5935590.835 569927.016 5935578.592 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_07573BB4-798C-43EB-9D6E-D02F79B1DCE8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569927.016 5935578.592 569898.702 5935567.541 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A6AE0505-DC97-4CED-A956-646EBA6AD850" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569898.702 5935567.541 569889.65 5935564.007 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9B5DC3CC-0FCE-4A8E-A960-BF04959BAF38" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569889.65 5935564.007 569852.189 5935549.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8B7B82B0-00EB-4C9D-ACC4-DE9EFA442FA8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569852.189 5935549.725 569815.032 5935534.788 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CD0B5B8D-67BC-4243-84CC-73189FCF41EB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569815.032 5935534.788 569809.8659 5935533.0967 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E7202182-14E0-4C3E-BB31-78796C4398B9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569809.8659 5935533.0967 569802.487 5935530.681 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7BE0B564-21F1-4C0E-BCB5-25201743D7FE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569802.487 5935530.681 569782.3991 5935524.1047 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7BEE3DAB-289B-4BEC-8F08-EE556DB90802" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569782.3991 5935524.1047 569757.725 5935516.027 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_058883BD-C957-4151-914A-574B57B25217" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569757.725 5935516.027 569707.595 5935502.147 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FC4F5D0B-130C-4705-860E-FF0FB616358A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569707.595 5935502.147 569682.885 5935497.12 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B3C959DC-6468-4F6C-AE24-E76FF5AD4A30" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569682.885 5935497.12 569674.348 5935495.002 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C7CC939C-256B-448C-B5E2-2356B7BDFA6B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569674.348 5935495.002 569654.498 5935490.799 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A75FB93-B158-48D6-B657-D384D04A2D68" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569654.498 5935490.799 569635.411 5935487.319 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9D71EB82-26E8-4CBA-B837-67CDB2B0B261" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569635.411 5935487.319 569635.2405 5935487.2883 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3D37EC97-52D6-40A0-8ECC-2A40F0BE58FE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569635.2405 5935487.2883 569625.5071 5935485.5347 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8329291D-9BD7-4841-BAE2-1845A6CB7C57" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569625.5071 5935485.5347 569625.3186 5935485.9195 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BCCB4992-D431-4217-B6BD-4604F68422A5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569625.3186 5935485.9195 569616.7196 5935425.7266 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9A200ECE-E75E-4AF9-B090-3F59ADC7D84F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569616.7196 5935425.7266 569422.912 5935401.9141 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_144B09A8-51E3-4D3B-A343-2DD9EB2EE183" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569422.912 5935401.9141 569416.528 5935460.741 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B6972357-2E7A-478D-A201-E5698F683410" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569416.528 5935460.741 569383.741 5935457.115 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3404F827-4706-498B-BCA5-4FFF99BEF4A7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569383.741 5935457.115 569391.8234 5935395.961 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BC5E48C8-C6C5-4E30-BD21-27D614278D8E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569391.8234 5935395.961 569337.5837 5935389.3464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B1966C50-5F7B-42CA-BE84-4394FC67DA50" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569337.5837 5935389.3464 569263.5002 5935374.7943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EAD00FFC-BF9F-448B-88B8-4BD26D0FB1C3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569263.5002 5935374.7943 569262.8387 5935346.3515 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D037C846-9362-4B45-AD41-78136964386B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569262.8387 5935346.3515 569253.5783 5935290.1274 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B4923FD5-4A63-4DFF-91A6-15362C7F7546" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569253.5783 5935290.1274 569230.4272 5935201.4918 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_23656883-66E8-42D7-B5F3-3A57247E5381" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569230.4272 5935201.4918 569342.2139 5935217.3669 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0D540F06-FE78-4F69-9114-B3B7C013C8C9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569342.2139 5935217.3669 569395.1307 5935214.721 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D0650DCE-362B-44AB-BECC-009EA4AEA93E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569395.1307 5935214.721 569386.5317 5935188.9241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B6E4563B-11D8-40D0-B1C1-459F79819AB1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569386.5317 5935188.9241 569367.3494 5934915.0798 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_617B591D-D90D-4EC0-A2DF-319FF4CC7B86" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569367.3494 5934915.0798 569313.1097 5934876.7151 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BE489AA9-4AE5-4771-A176-AF728C1E9AEC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569313.1097 5934876.7151 569313.1097 5934665.7095 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A74215E8-07B2-46DB-BBD5-81B982B9F4B7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569313.1097 5934665.7095 569323.693 5934653.1418 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_13CAC81A-E4E1-4274-87CD-CF173A6E499E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569323.693 5934653.1418 569340.891 5934658.4334 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_44242CAA-171B-4B74-A848-D6E7007EAEA7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569340.891 5934658.4334 569365.365 5934666.371 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BCC57881-C01C-4150-B00B-7FA0F092ACCD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569365.365 5934666.371 569391.1619 5934668.3553 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1476A729-9FC5-442E-AAC7-B475CEABEA3D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569391.1619 5934668.3553 569423.5734 5934665.7095 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D26A993E-5D8A-4915-9C47-1A8B46D444FD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569423.5734 5934665.7095 569508.2403 5934659.7564 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4CE3E6C0-6AC1-4246-B659-4534D60863ED" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569508.2403 5934659.7564 569560.4956 5934662.4022 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A1C66A98-3189-4457-8886-66309D7E1907" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569560.4956 5934662.4022 569604.8134 5934672.9856 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1DB4D83E-760A-47E3-897D-D0B683615CFC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569604.8134 5934672.9856 569609.5671 5934663.4148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_65681E73-5E80-4161-905B-DC9648195A82" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569609.5671 5934663.4148 569610.172928846 5934663.65695647 569610.778 5934663.901 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C7CB8C67-9285-4014-ABCC-F41A35783438" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569610.778 5934663.901 569620.497 5934642.516 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1ABE3961-AB46-474C-9B55-DE04D7FB4C2E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569620.497 5934642.516 569632.983 5934643.015 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FE2344BE-91F6-43BB-A20B-797535E50127" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569632.983 5934643.015 569652.261 5934648.647 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F11D8FF6-1553-4539-8D4D-F23454487D90" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569652.261 5934648.647 569678.196 5934656.226 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D87474B9-2EFB-4B27-B337-55171D9D8432" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569678.196 5934656.226 569689.513 5934659.534 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_617004FD-4DA8-4F77-A44F-6E554D5A64F0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569689.513 5934659.534 569737.139 5934664.504 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FA1F1043-AD61-42A4-B60B-4A4D9077B087" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569737.139 5934664.504 569757.65 5934664.844 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_78DCBF78-2DE9-491B-B52D-50077FB06C68" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569757.65 5934664.844 569761.984 5934653.458 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A3A452D3-BAFE-4747-8978-A8975E8A3633" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569761.984 5934653.458 569767.540203225 5934633.48048933 569777.23 5934615.148 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_73052AAF-53DB-466B-8CC5-8D683F61EC6B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569777.23 5934615.148 569778.796034083 5934613.03609136 569780.565 5934611.091 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_1DA1EFAC-A002-4EBF-B9EA-A0D0253E1279" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">569780.565 5934611.091 569789.544642533 5934603.32786963 569799.2186 5934596.4493 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4005F834-054F-4A81-A2B1-417EBB27BC6B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569799.2186 5934596.4493 569801.2669 5934594.9333 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_503C241A-3604-4018-AAA1-BB8C7D662489" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569801.2669 5934594.9333 569874.6889 5934548.6311 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_89829C4E-1D41-4E3F-9701-5C6CFEE219F5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">569874.6889 5934548.6311 570012.2725 5934524.8186 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_586F4DC5-8ED5-4ABE-ABE3-1A7A45BAE870" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570012.2725 5934524.8186 570099.5852 5934520.1884 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_608BF5AC-F6D6-4B62-AC52-18B99D5CA2FF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570099.5852 5934520.1884 570301.9918 5934487.7769 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_819B8FA2-6EA3-486A-B9B6-D80DC478D4CF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570301.9918 5934487.7769 570473.3099 5934446.1049 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B2991801-A663-4DAE-945D-3ED44B8B8F90" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570473.3099 5934446.1049 570469.296 5934431.583 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D1FAF39-CE79-49F8-81BF-1C0054205C21" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570469.296 5934431.583 570455.282 5934397.443 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A47F1122-DCCB-48D0-B517-FE4998EEECDB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570455.282 5934397.443 570452.496 5934391.2454 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_97C2798A-8699-40E3-B4ED-D45395529B70" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570452.496 5934391.2454 570450.61 5934387.05 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C347AFD4-CF47-4702-AFBD-073B6A085BBF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570450.61 5934387.05 570451.053 5934386.865 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC985AA7-45AA-45D8-BB9D-CF8CFF2B7446" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570451.053 5934386.865 570448.733 5934381.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8A89BD9C-C5BB-4767-8B94-734DC8A5A274" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570448.733 5934381.725 570441.101 5934365.283 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_54469495-D5C7-4775-8D10-717B570A805A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570441.101 5934365.283 570454.186 5934362.003 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_72B74D53-6790-4AE1-815A-1DABCBFC87BB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570454.186 5934362.003 570461.075 5934360.277 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E6D8E5DD-2F1A-40DA-8BF9-7D201F1B4E0E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570461.075 5934360.277 570448.937 5934334.012 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D2ECF0AC-4B0B-4A49-99E6-2AEF04D358D6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570448.937 5934334.012 570433.249 5934337.943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_755EE5E2-1F49-4655-BE2C-7C6232199670" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">570433.249 5934337.943 570416.681 5934296.146 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:gebietsArt>17000</xplan:gebietsArt>
    </xplan:SO_Gebiet>
  </gml:featureMember>
</xplan:XPlanAuszug>
