<?xml version="1.0"?>
<!--
  #%L
  xplan-manager-config - Modul zur Gruppierung der Konfiguration
  %%
  Copyright (C) 2008 - 2023 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<XPlanAuszug xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xsd="http://www.w3.org/2001/XMLSchema" gml:id="Gml_913129EA-877B-4EF1-8D0B-D353B4B51E87" xmlns="http://www.xplanung.de/xplangml/4/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>572769.5767 5932947.209</gml:lowerCorner>
      <gml:upperCorner>573006.5734 5933173.5017</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <BP_Plan gml:id="Gml_6E7847BE-04C1-4A3D-9BB9-9505D10E12E7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572769.5767 5932947.209</gml:lowerCorner>
          <gml:upperCorner>573006.5734 5933173.5017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <name>Billstedt28</name>
      <technHerstellDatum>2015-07-29</technHerstellDatum>
      <xPlanGMLVersion>4.1</xPlanGMLVersion>
      <raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_74BFE234-AD62-4658-A6CF-CA7806986A5F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572878.208 5933129.1596 572989.6338 5933070.213 573006.5734 5933061.2509 
572994.336 5933040.0632 572957.6953 5932963.8529 572951.079 5932950.0947 
572949.7278 5932947.209 572912.4143 5932965.1798 572837.3411 5933022.9406 
572793.6617 5933055.9478 572769.5767 5933072.5548 572778.71 5933087.887 
572787.763 5933105.442 572799.4829 5933126.7177 572801.7796 5933130.4908 
572790.0917 5933137.683 572788.0455 5933140.1586 572786.4289 5933142.6342 
572785.4605 5933144.5749 572784.703 5933146.621 572784.492 5933148.2337 
572784.5936 5933149.6355 572784.9215 5933151.0373 572794.2578 5933170.214 
572795.8548 5933173.5017 572799.7758 5933171.3889 572878.208 5933129.1596 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </raeumlicherGeltungsbereich>
      <texte xlink:href="#Gml_D0BAF8F2-B3A5-49E2-925F-A6FB1269A79F" />
      <texte xlink:href="#Gml_776BD1F9-6319-4335-9096-AA5CAF5B1E16" />
      <texte xlink:href="#Gml_F17C05E5-F1F4-40BD-86CD-2B0A4432B657" />
      <texte xlink:href="#Gml_ABF7EAC4-A651-4CC4-B793-AAFCBD2C20B5" />
      <begruendungsTexte xlink:href="#Gml_4FE2C4E6-E3D5-4273-A963-75AC354F3862" />
      <gemeinde>
        <XP_Gemeinde>
          <ags>02000000</ags>
          <gemeindeName>Freie und Hansestadt Hamburg</gemeindeName>
          <ortsteilName>131</ortsteilName>
        </XP_Gemeinde>
      </gemeinde>
      <planArt>1000</planArt>
      <verfahren>1000</verfahren>
      <rechtsstand>3000</rechtsstand>
      <rechtsverordnungsDatum>1968-05-10</rechtsverordnungsDatum>
      <bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
    </BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="Gml_D0BAF8F2-B3A5-49E2-925F-A6FB1269A79F">
      <schluessel>§2 Nr.1</schluessel>
      <text>Im Gewerbegebiet an der Straße Hauskoppel sind Betriebe und Anlagen mit erheblichem Zu- und Abfahrtsverkehr, insbesondere Tankstellen, Fuhrunternehmen, Lagerplätze und Lagerhäuser, unzulässig.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="Gml_776BD1F9-6319-4335-9096-AA5CAF5B1E16">
      <schluessel>§2 Nr.2</schluessel>
      <text>Die festgesetzten Leitungsrechte auf den Flurstücken 1433, 1514 und 1969 der Gemarkung Schiffbek umfassen die Befugnis der Freien und Hansestadt Hamburg, unterirdische öffentliche Sielanlagen herzustellen und zu unterhalten. 2,0 m beiderseits der Sielachsen sind bauliche Vorhaben und solche Nutzungen unzulässig, welche die Unterhaltung beeinträchtigen können. Das festgesetzte Geh- und Fahrrecht auf den Flurstücken 1422, 1424, 1425 und 1804 der Gemarkung Schiffbek umfaßt die Befugnis, für den Anschluß der Flurstücke 1422 und 1425 an die Straße Kirchlinden eine Zufahrt anzulegen und zu unterhalten.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="Gml_F17C05E5-F1F4-40BD-86CD-2B0A4432B657">
      <schluessel>§2 Nr.3</schluessel>
      <text>Die Stellflächen für Kraftfahrzeuge dienen zur Erfüllung der Verpflichtungen nach der Verordnung über Garagen und Einstellplätze vom 17. Februar 1939 (Reichsgesetzblatt I Seite 219) im Wohngebiet geschlossener Bauweise, und zwar in erster Linie für die Baugrundstücke, auf denen sie ausgewiesen sind. Die Flächen dürfen als Einstellplätze und als Garagen unter Erdgleiche genutzt werden. Eingeschossige Garagen sind zulässig, wenn die benachbarte Bebauung und ihre Nutzung nicht beeinträchtigt werden. Auch die nicht überbaubaren Grundstücksteile sind als Garagen unter Erdgleiche nutzbar, wenn Wohnruhe und Gartenanlagen nicht erheblich beeinträchtigt werden.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="Gml_ABF7EAC4-A651-4CC4-B793-AAFCBD2C20B5">
      <schluessel>§2 Nr.4</schluessel>
      <text>Soweit der Bebauungsplan keine besonderen Bestimmungen trifft, gelten die Verordnung über die bauliche Nutzung der Grundstücke (Baunutzungsverordnung) vom 26. Juni 1962 (Bundesgesetzblatt I Seite 429) und die Baupolizeiverordnung für die Freie und Hansestadt Hamburg vom 8. Juni 1938 (Sammlung des bereinigten hamburgischen Landesrechts 21302-n). § 7 Absatz 4 des Hamburgischen Wegegesetzes vom 4. April 1961 (Hamburgisches Gesetz- und Verordnungsblatt Seite 117) findet keine Anwendung. </text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_BegruendungAbschnitt gml:id="Gml_4FE2C4E6-E3D5-4273-A963-75AC354F3862">
      <refText>
        <XP_ExterneReferenz>
          <beschreibung>Der Bebauungsplan Billstedt 28 für das Plangebiet Billstedter Hauptstraße — Westgrenze des Flurstücks 1512 der Gemarkung Schiffbek — Kreuzkirchenstieg — Hauskoppel — Nordgrenzen der Flurstücke 1462, 1461, 1442 und 1440 der Gemarkung Schiffbek — Schiffbeker Weg (Bezirk Hamburg-Mitte, Ortsteil 131) wird festgestellt.</beschreibung>
        </XP_ExterneReferenz>
      </refText>
    </XP_BegruendungAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <BP_Bereich gml:id="Gml_963B9622-6285-42AC-B231-296F65C370E3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572769.5767 5932947.209</gml:lowerCorner>
          <gml:upperCorner>573006.5734 5933173.5017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <nummer>0</nummer>
      <geltungsbereich>
        <gml:Polygon gml:id="Gml_24A5E9F5-6541-499D-B187-EC56FA849DD2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572878.208 5933129.1596 572989.6338 5933070.213 573006.5734 5933061.2509 
572994.336 5933040.0632 572957.6953 5932963.8529 572951.079 5932950.0947 
572949.7278 5932947.209 572912.4143 5932965.1798 572837.3411 5933022.9406 
572793.6617 5933055.9478 572769.5767 5933072.5548 572778.71 5933087.887 
572787.763 5933105.442 572799.4829 5933126.7177 572801.7796 5933130.4908 
572790.0917 5933137.683 572788.0455 5933140.1586 572786.4289 5933142.6342 
572785.4605 5933144.5749 572784.703 5933146.621 572784.492 5933148.2337 
572784.5936 5933149.6355 572784.9215 5933151.0373 572794.2578 5933170.214 
572795.8548 5933173.5017 572799.7758 5933171.3889 572878.208 5933129.1596 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </geltungsbereich>
      <rasterBasis xlink:href="#Gml_B47BCDBE-AB8E-40C6-AD2C-895AE9E7807E" />
      <versionBauNVO>1000</versionBauNVO>
      <gehoertZuPlan xlink:href="#Gml_6E7847BE-04C1-4A3D-9BB9-9505D10E12E7" />
      <inhaltBPlan xlink:href="#Gml_932C14E1-BCB6-4261-B489-753FC01BA078" />
      <inhaltBPlan xlink:href="#Gml_B06F0122-8AA6-4190-B815-879670650B59" />
      <inhaltBPlan xlink:href="#Gml_1C804B4E-EB24-4FB1-B8E2-825096BF4341" />
      <inhaltBPlan xlink:href="#Gml_A48CA307-17CB-413E-9E15-340D1F12B4C3" />
      <inhaltBPlan xlink:href="#Gml_A4168880-AC4A-41ED-B233-028EA0A43B06" />
      <inhaltBPlan xlink:href="#Gml_EBF868A9-8E30-4504-A02C-8BBE70AC322B" />
      <inhaltBPlan xlink:href="#Gml_8C824CF2-3E06-418F-AD42-61DD768A2B08" />
      <inhaltBPlan xlink:href="#Gml_35BD673C-CD52-4381-AD33-1C3F76647204" />
      <inhaltBPlan xlink:href="#Gml_77A07A92-CEB5-4C14-8A25-CE1EBEA89B70" />
      <inhaltBPlan xlink:href="#Gml_44CC6527-2A62-44BE-A0CD-3D16EEFFA341" />
      <inhaltBPlan xlink:href="#Gml_D5AC452D-324C-46A7-AA97-101EB931542B" />
      <inhaltBPlan xlink:href="#Gml_F9EA02CC-593A-4A34-B7BF-0974F25C65A4" />
      <inhaltBPlan xlink:href="#Gml_395F5B87-59FB-4A20-B383-E074543B0450" />
      <inhaltBPlan xlink:href="#Gml_992BC080-B84F-4B63-8001-5308F13E5141" />
      <inhaltBPlan xlink:href="#Gml_F0393AED-7A1A-4D76-BFD9-ADF98ADABCE6" />
      <inhaltBPlan xlink:href="#Gml_447F0ACF-B59A-4E41-B61B-36C1A7CAED80" />
      <inhaltBPlan xlink:href="#Gml_4C8D2CCA-036C-4B5C-8A09-CE488D11FAC6" />
      <inhaltBPlan xlink:href="#Gml_7B02B3D6-A8A9-437E-BDB5-F395F1452BAD" />
      <inhaltBPlan xlink:href="#Gml_514EB09E-466C-41C2-90DC-C0956FB2739D" />
      <inhaltBPlan xlink:href="#Gml_E9757F14-1B1E-4258-8135-2D205E3772D2" />
      <inhaltBPlan xlink:href="#Gml_A813544B-F180-4F2B-9382-C8095C21A79F" />
      <inhaltBPlan xlink:href="#Gml_CB0CDCA0-379B-400A-80A2-5C28E40B060F" />
      <inhaltBPlan xlink:href="#Gml_76F06319-BADE-44A5-979C-26BF2CC7B4F0" />
      <inhaltBPlan xlink:href="#Gml_892C8095-2FE6-4B90-A41A-CE1592D9E9E0" />
      <inhaltBPlan xlink:href="#Gml_C6139F30-47EE-42F6-922F-2B4719709C29" />
      <inhaltBPlan xlink:href="#Gml_6D6C541C-1131-43E1-9381-DCA6EDE531D0" />
      <inhaltBPlan xlink:href="#Gml_0A62CF84-69BA-4A34-9C3C-5EB9CC8FDA06" />
      <inhaltBPlan xlink:href="#Gml_83EC7B94-DC30-406F-8CE1-779AE3FE6BD4" />
      <inhaltBPlan xlink:href="#Gml_F8C46F65-12A8-4F60-84EC-552478FF41FF" />
      <inhaltBPlan xlink:href="#Gml_B7654486-7D12-4FEA-9D45-ACCB5F286CA8" />
      <inhaltBPlan xlink:href="#Gml_D5434748-B8A0-4AD5-8E63-CDABA5A2FF7D" />
      <inhaltBPlan xlink:href="#Gml_C731F60C-4D53-49F4-BC73-2823204F3469" />
      <inhaltBPlan xlink:href="#Gml_C0788E80-F5C3-4F59-9C49-D1D9D9F86082" />
      <inhaltBPlan xlink:href="#Gml_9F8132C9-585F-498A-9341-7C94E24180A9" />
      <inhaltBPlan xlink:href="#Gml_A781C43F-2A86-46CA-B5A3-99F71378B31C" />
      <inhaltBPlan xlink:href="#Gml_71DBC293-17DE-45EF-BF31-E9EE0E6A075D" />
      <inhaltBPlan xlink:href="#Gml_1999096A-1103-4DF8-AA4E-3BCEC07DE494" />
      <inhaltBPlan xlink:href="#Gml_AC8E798A-DEA1-4F1C-8546-E6C5034A1103" />
      <inhaltBPlan xlink:href="#Gml_E3715B1E-DF0F-496A-BC89-DCFED9DCBA59" />
    </BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <BP_GemeinbedarfsFlaeche gml:id="Gml_932C14E1-BCB6-4261-B489-753FC01BA078">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572778.71 5933075.1873</gml:lowerCorner>
          <gml:upperCorner>572847.111 5933159.839</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <text>Verwaltung und Altentagesstätte</text>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_64416997-C117-499E-9BF0-42C5EE598CA7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572801.015 5933136.598 572813.34 5933159.839 572845.342 5933143.467 
572847.111 5933142.519 572844.414 5933141.72 572835.966 5933125.837 
572833.767 5933122.237 572830.506 5933119.786 572827.505 5933118.985 
572826.444 5933118.701 572822.301 5933119.064 572819.579 5933118.288 
572798.874 5933079.138 572796.3144 5933075.1873 572788.302 5933081.048 
572786.552 5933082.295 572784.683 5933083.627 572778.71 5933087.887 
572787.763 5933105.442 572799.4829 5933126.7177 572801.7796 5933130.4908 
572798.5743 5933132.4632 572801.015 5933136.598 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <zweckbestimmung>1000</zweckbestimmung>
      <zweckbestimmung>1600</zweckbestimmung>
      <besondereZweckbestimmung>10003</besondereZweckbestimmung>
      <besondereZweckbestimmung>16003</besondereZweckbestimmung>
      <zugunstenVon>Freie und Hansestadt Hamburg</zugunstenVon>
    </BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="Gml_B06F0122-8AA6-4190-B815-879670650B59">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572788.1909 5933065.3615</gml:lowerCorner>
          <gml:upperCorner>572788.1909 5933065.3615</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>3000</bezugspunkt>
          <h uom="m">15.8</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Point gml:id="Gml_080CDA36-3D67-4741-AA89-EFDDEE930E26" srsName="EPSG:25832">
          <gml:pos>572788.1909 5933065.3615</gml:pos>
        </gml:Point>
      </position>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="Gml_1C804B4E-EB24-4FB1-B8E2-825096BF4341">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572951.4959 5932950.9617</gml:lowerCorner>
          <gml:upperCorner>572951.4959 5932950.9617</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>3000</bezugspunkt>
          <h uom="m">16.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Point gml:id="Gml_4B7E7FB5-CC0C-46D5-B8E9-A140362EEF40" srsName="EPSG:25832">
          <gml:pos>572951.4959 5932950.9617</gml:pos>
        </gml:Point>
      </position>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="Gml_A48CA307-17CB-413E-9E15-340D1F12B4C3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>573006.5734 5933061.2509</gml:lowerCorner>
          <gml:upperCorner>573006.5734 5933061.2509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>3000</bezugspunkt>
          <h uom="m">17.1</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Point gml:id="Gml_AEE3A52C-5003-4F5C-87DA-7BF361B1FBA7" srsName="EPSG:25832">
          <gml:pos>573006.5734 5933061.2509</gml:pos>
        </gml:Point>
      </position>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="Gml_A4168880-AC4A-41ED-B233-028EA0A43B06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572797.4462 5933172.6442</gml:lowerCorner>
          <gml:upperCorner>572797.4462 5933172.6442</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>3000</bezugspunkt>
          <h uom="m">16.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Point gml:id="Gml_628CC6FB-DA4C-4783-923E-3A527696DAD8" srsName="EPSG:25832">
          <gml:pos>572797.4462 5933172.6442</gml:pos>
        </gml:Point>
      </position>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_VerEntsorgung gml:id="Gml_EBF868A9-8E30-4504-A02C-8BBE70AC322B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572791.424 5933078.7644</gml:lowerCorner>
          <gml:upperCorner>572848.1262 5933141.9759</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>-1</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_7E5FE11A-C65F-483F-AA19-F4ECC86A680A" srsName="EPSG:25832">
          <gml:posList>572848.1262 5933141.9759 572815.7482 5933119.7159 572796.3013 5933082.2772 
572791.424 5933078.7644 </gml:posList>
        </gml:LineString>
      </position>
      <zweckbestimmung>1800</zweckbestimmung>
      <besondereZweckbestimmung>18000</besondereZweckbestimmung>
    </BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <BP_VerEntsorgung gml:id="Gml_8C824CF2-3E06-418F-AD42-61DD768A2B08">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572794.7699 5933076.317</gml:lowerCorner>
          <gml:upperCorner>572849.5806 5933141.1979</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>-1</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_75D8937A-27AB-433F-9F59-706490991108" srsName="EPSG:25832">
          <gml:posList>572849.5806 5933141.1979 572817.0711 5933118.7898 572794.7699 5933076.317 
</gml:posList>
        </gml:LineString>
      </position>
      <zweckbestimmung>1800</zweckbestimmung>
      <besondereZweckbestimmung>18000</besondereZweckbestimmung>
    </BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <BP_Wegerecht gml:id="Gml_35BD673C-CD52-4381-AD33-1C3F76647204">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572839.0525 5933131.64</gml:lowerCorner>
          <gml:upperCorner>572851.9323 5933142.519</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <refTextInhalt xlink:href="#Gml_776BD1F9-6319-4335-9096-AA5CAF5B1E16" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_F60FF62A-C6C1-4605-9FCA-5E54C7F5BAC9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572851.9323 5933139.9399 572839.0525 5933131.64 572840.985 5933135.2731 
572842.4886 5933138.1 572844.414 5933141.72 572847.111 5933142.519 
572848.1262 5933141.9759 572849.5806 5933141.1979 572851.7337 5933140.0461 
572851.9323 5933139.9399 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <typ>4000</typ>
    </BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BaugebietsTeilFlaeche gml:id="Gml_77A07A92-CEB5-4C14-8A25-CE1EBEA89B70">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572796.3144 5933057.868</gml:lowerCorner>
          <gml:upperCorner>572854.131 5933119.064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_1790C8AE-A059-469A-A641-ACB1A78F4593" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572822.301 5933119.064 572826.444 5933118.701 572827.505 5933118.985 
572830.939 5933116.908 572842.292 5933110.058 572854.131 5933102.915 
572846.734 5933093.471 572818.853 5933057.868 572804.411 5933069.265 
572796.3144 5933075.1873 572798.874 5933079.138 572819.579 5933118.288 
572822.301 5933119.064 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <GFZ>0.7</GFZ>
      <GRZ>0.5</GRZ>
      <Z>2</Z>
      <allgArtDerBaulNutzung>3000</allgArtDerBaulNutzung>
      <besondereArtDerBaulNutzung>1700</besondereArtDerBaulNutzung>
    </BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BaugebietsTeilFlaeche gml:id="Gml_44CC6527-2A62-44BE-A0CD-3D16EEFFA341">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572827.505 5933049.227</gml:lowerCorner>
          <gml:upperCorner>572976.073 5933142.519</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_5206FE4A-11A3-404C-9C35-415AE0AEFD1A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572957.92 5933083.197 572976.073 5933073.475 572975.641 5933072.619 
572963.857 5933049.227 572961.695 5933050.298 572952.362 5933054.918 
572944.746 5933058.689 572943.575 5933056.507 572926.506 5933065.541 
572923.838 5933060.014 572890.431 5933081.002 572884.351 5933068.398 
572846.734 5933093.471 572854.131 5933102.915 572842.292 5933110.058 
572830.939 5933116.908 572827.505 5933118.985 572830.506 5933119.786 
572833.767 5933122.237 572835.966 5933125.837 572844.414 5933141.72 
572847.111 5933142.519 572856.8 5933137.336 572900.922 5933113.727 
572906.145 5933110.93 572939.732 5933092.94 572957.92 5933083.197 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <allgArtDerBaulNutzung>1000</allgArtDerBaulNutzung>
      <besondereArtDerBaulNutzung>1100</besondereArtDerBaulNutzung>
      <bauweise>2000</bauweise>
    </BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BereichOhneEinAusfahrtLinie gml:id="Gml_D5AC452D-324C-46A7-AA97-101EB931542B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572943.4893 5932978.0656</gml:lowerCorner>
          <gml:upperCorner>572987.266 5933067.1283</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_F01AB91C-F27C-4ACB-BAF5-3952BB7338D5" srsName="EPSG:25832">
          <gml:posList>572943.4893 5932978.0656 572987.266 5933067.1283 </gml:posList>
        </gml:LineString>
      </position>
      <typ>3000</typ>
    </BP_BereichOhneEinAusfahrtLinie>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_F9EA02CC-593A-4A34-B7BF-0974F25C65A4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572783.7781 5933093.7946</gml:lowerCorner>
          <gml:upperCorner>572801.3432 5933118.9359</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <text>geschlossene Bauweise</text>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_82BC7714-23D9-4ADB-8C93-0AB53DE56BFD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572795.1962 5933118.9359 572801.3432 5933115.9198 572790.8773 5933093.7946 
572783.7781 5933097.7147 572787.763 5933105.442 572795.1962 5933118.9359 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>1</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_395F5B87-59FB-4A20-B383-E074543B0450">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572807.8073 5933132.2209</gml:lowerCorner>
          <gml:upperCorner>572835.658 5933156.5539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_1286CBCA-3A10-44ED-B206-CA01AB87B9A2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572835.658 5933144.719 572829.571 5933133.149 572827.2458 5933134.3723 
572826.1139 5933132.2209 572821.0489 5933134.8855 572822.1807 5933137.037 
572807.8073 5933144.5988 572813.9491 5933156.5539 572835.658 5933144.719 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_992BC080-B84F-4B63-8001-5308F13E5141">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572800.261 5933063.632</gml:lowerCorner>
          <gml:upperCorner>572847.075 5933117.6071</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_B68E193E-5D69-4699-914C-4CCF03EB8716" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572821.3574 5933117.6071 572847.075 5933103.9546 572837.5499 5933085.6454 
572819.7699 5933063.632 572800.261 5933078.4046 572821.3574 5933117.6071 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="Gml_F0393AED-7A1A-4D76-BFD9-ADF98ADABCE6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572800.261 5933063.632</gml:lowerCorner>
          <gml:upperCorner>572847.075 5933117.6071</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_C023788C-EA68-45CB-9FB6-9C0324EC1D85" srsName="EPSG:25832">
          <gml:posList>572821.3574 5933117.6071 572847.075 5933103.9546 572837.5499 5933085.6454 
572819.7699 5933063.632 572800.261 5933078.4046 572821.3574 5933117.6071 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="Gml_447F0ACF-B59A-4E41-B61B-36C1A7CAED80">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572783.7781 5933093.7946</gml:lowerCorner>
          <gml:upperCorner>572801.3432 5933118.9359</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_B8584786-36F2-465C-8381-BA0607A8C7FE" srsName="EPSG:25832">
          <gml:posList>572795.1962 5933118.9359 572801.3432 5933115.9198 572790.8773 5933093.7946 
572783.7781 5933097.7147 572787.763 5933105.442 572795.1962 5933118.9359 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="Gml_4C8D2CCA-036C-4B5C-8A09-CE488D11FAC6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572807.8073 5933132.2209</gml:lowerCorner>
          <gml:upperCorner>572835.658 5933156.5539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_3EE373D7-5443-40E4-B8D5-20FC84410CC9" srsName="EPSG:25832">
          <gml:posList>572835.658 5933144.719 572829.571 5933133.149 572827.2458 5933134.3723 
572826.1139 5933132.2209 572821.0489 5933134.8855 572822.1807 5933137.037 
572807.8073 5933144.5988 572813.9491 5933156.5539 572835.658 5933144.719 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_7B02B3D6-A8A9-437E-BDB5-F395F1452BAD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572863.229 5933085.456</gml:lowerCorner>
          <gml:upperCorner>572889.563 5933121.471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_580D06F7-D085-4D3E-B5B3-8AF702379A65" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572879.612 5933121.471 572889.563 5933116.159 572873.163 5933085.456 
572863.229 5933090.759 572879.612 5933121.471 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>4</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_514EB09E-466C-41C2-90DC-C0956FB2739D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572901.527 5933062.692</gml:lowerCorner>
          <gml:upperCorner>572969.1475 5933104.711</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_F6052F56-A223-44C9-A271-FAA91331B827" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572906.722 5933104.711 572937.415 5933088.55 572938.235 5933089.861 
572969.1475 5933073.127 572963.4987 5933062.692 572932.801 5933079.156 
572932.502 5933078.509 572901.527 5933094.86 572906.722 5933104.711 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>3</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauLinie gml:id="Gml_E9757F14-1B1E-4258-8135-2D205E3772D2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572863.229 5933085.456</gml:lowerCorner>
          <gml:upperCorner>572889.563 5933121.471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_CB25E591-AB25-4CD9-B567-68F9895FC500" srsName="EPSG:25832">
          <gml:posList>572879.612 5933121.471 572889.563 5933116.159 572873.163 5933085.456 
572863.229 5933090.759 572879.612 5933121.471 </gml:posList>
        </gml:LineString>
      </position>
    </BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauLinie gml:id="Gml_A813544B-F180-4F2B-9382-C8095C21A79F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572901.527 5933062.692</gml:lowerCorner>
          <gml:upperCorner>572969.1475 5933104.711</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_63AF0240-7E66-449D-A6AE-E3E1E15BD444" srsName="EPSG:25832">
          <gml:posList>572906.722 5933104.711 572937.415 5933088.55 572938.235 5933089.861 
572969.1475 5933073.127 572963.4987 5933062.692 572932.801 5933079.156 
572932.502 5933078.509 572901.527 5933094.86 572906.722 5933104.711 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BaugebietsTeilFlaeche gml:id="Gml_CB0CDCA0-379B-400A-80A2-5C28E40B060F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572818.853 5932980.589</gml:lowerCorner>
          <gml:upperCorner>572984.608 5933093.471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_D97F3C2B-CFAC-4300-A7BE-2F211FA46914" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572846.734 5933093.471 572884.351 5933068.398 572890.431 5933081.002 
572923.838 5933060.014 572926.506 5933065.541 572943.575 5933056.507 
572944.746 5933058.689 572952.362 5933054.918 572961.695 5933050.298 
572963.857 5933049.227 572975.641 5933072.619 572983.571 5933068.375 
572984.608 5933065.1117 572967.6565 5933030.8269 572966.5346 5933031.2787 
572942.8373 5932982.9248 572939.393 5932984.848 572937.002 5932980.589 
572929.449 5932984.333 572894.532 5933001.64 572883.046 5933007.215 
572873.638 5933014.638 572862.614 5933023.337 572818.853 5933057.868 
572846.734 5933093.471 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <allgArtDerBaulNutzung>2000</allgArtDerBaulNutzung>
      <besondereArtDerBaulNutzung>1600</besondereArtDerBaulNutzung>
      <bauweise>2000</bauweise>
    </BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_76F06319-BADE-44A5-979C-26BF2CC7B4F0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572901.7749 5932998.8382</gml:lowerCorner>
          <gml:upperCorner>572939.8134 5933022.2713</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_CACE4545-444B-4E5B-B92C-08C7DD3222DF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572905.7417 5933022.2713 572939.8134 5933009.098 572935.8466 5932998.8382 
572901.7749 5933012.0115 572905.7417 5933022.2713 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>4</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_892C8095-2FE6-4B90-A41A-CE1592D9E9E0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572831.2545 5933021.2386</gml:lowerCorner>
          <gml:upperCorner>572908.488 5933075.0698</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_8F5DC2CB-B909-40E3-96C1-0D160371AA01" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572836.5152 5933075.0698 572877.6606 5933054.53 572908.488 5933039.141 
572900.999 5933024.105 572899.5713 5933021.2386 572868.9705 5933036.5146 
572868.7189 5933036.6402 572872.2781 5933043.8049 572835.349 5933062.2398 
572831.2545 5933064.2838 572836.5152 5933075.0698 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>4</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_C6139F30-47EE-42F6-922F-2B4719709C29">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572954.0956 5933035.9279</gml:lowerCorner>
          <gml:upperCorner>572963.4637 5933050.7532</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_D3C7CEA3-619E-4307-800E-73A2C1CF8D5F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572963.4637 5933049.4218 572956.7919 5933035.9279 572955.7646 5933036.429 
572954.0956 5933037.2432 572960.7754 5933050.7532 572961.695 5933050.298 
572963.4637 5933049.4218 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>1</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_NutzungsartenGrenze gml:id="Gml_6D6C541C-1131-43E1-9381-DCA6EDE531D0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572947.15 5933011.9002</gml:lowerCorner>
          <gml:upperCorner>572957.0376 5933016.7206</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_3461EEB2-D16F-451C-A0C1-C433B3EA7635" srsName="EPSG:25832">
          <gml:posList>572947.15 5933016.7206 572957.0376 5933011.9002 </gml:posList>
        </gml:LineString>
      </position>
      <typ>9999</typ>
    </BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_0A62CF84-69BA-4A34-9C3C-5EB9CC8FDA06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572831.7714 5932980.589</gml:lowerCorner>
          <gml:upperCorner>572957.0376 5933062.2398</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_4F9A6051-E2B3-45F8-9423-F245DD499F63" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572835.349 5933062.2398 572872.2781 5933043.8049 572868.7189 5933036.6402 
572868.9705 5933036.5146 572899.5713 5933021.2386 572900.999 5933024.105 
572905.7417 5933022.2713 572901.7749 5933012.0115 572935.8466 5932998.8382 
572939.8134 5933009.098 572942.8407 5933007.9276 572947.15 5933016.7206 
572957.0376 5933011.9002 572942.8373 5932982.9248 572939.393 5932984.848 
572937.002 5932980.589 572929.449 5932984.333 572894.532 5933001.64 
572887.5433 5933005.0321 572892.413 5933014.7603 572859.1516 5933031.4102 
572863.1502 5933039.4201 572831.7714 5933055.0844 572835.349 5933062.2398 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>1</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_83EC7B94-DC30-406F-8CE1-779AE3FE6BD4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572946.5497 5933011.9002</gml:lowerCorner>
          <gml:upperCorner>572981.0389 5933062.6196</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_00BE0708-BF81-4B00-BA5B-A5C378E762C4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572947.15 5933016.7206 572946.5497 5933017.1799 572955.7646 5933036.429 
572956.7919 5933035.9279 572963.4637 5933049.4218 572963.857 5933049.227 
572964.808 5933048.7562 572971.6625 5933062.6196 572981.0389 5933057.8931 
572967.6565 5933030.8269 572966.5346 5933031.2787 572957.0376 5933011.9002 
572947.15 5933016.7206 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Zzwingend>4</Zzwingend>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_NutzungsartenGrenze gml:id="Gml_F8C46F65-12A8-4F60-84EC-552478FF41FF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572955.7646 5933035.9279</gml:lowerCorner>
          <gml:upperCorner>572963.4637 5933049.4218</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_D2014790-2B86-4209-BFAB-E12796C55FD3" srsName="EPSG:25832">
          <gml:posList>572963.4637 5933049.4218 572956.7919 5933035.9279 572955.7646 5933036.429 
</gml:posList>
        </gml:LineString>
      </position>
      <typ>9999</typ>
    </BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_NutzungsartenGrenze gml:id="Gml_B7654486-7D12-4FEA-9D45-ACCB5F286CA8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572901.7749 5932998.8382</gml:lowerCorner>
          <gml:upperCorner>572939.8134 5933022.2713</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_605DA007-04F7-4D9F-B3BF-43706B9DA2ED" srsName="EPSG:25832">
          <gml:posList>572939.8134 5933009.098 572935.8466 5932998.8382 572901.7749 5933012.0115 
572905.7417 5933022.2713 </gml:posList>
        </gml:LineString>
      </position>
      <typ>9999</typ>
    </BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_NutzungsartenGrenze gml:id="Gml_D5434748-B8A0-4AD5-8E63-CDABA5A2FF7D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572835.349 5933021.2386</gml:lowerCorner>
          <gml:upperCorner>572900.999 5933062.2398</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_6F78D779-FCAA-4E1B-B5CA-5391C8046D54" srsName="EPSG:25832">
          <gml:posList>572900.999 5933024.105 572899.5713 5933021.2386 572868.9705 5933036.5146 
572868.7189 5933036.6402 572872.2781 5933043.8049 572835.349 5933062.2398 
</gml:posList>
        </gml:LineString>
      </position>
      <typ>9999</typ>
    </BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauLinie gml:id="Gml_C731F60C-4D53-49F4-BC73-2823204F3469">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572831.2545 5932980.589</gml:lowerCorner>
          <gml:upperCorner>572981.0389 5933075.0698</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_68D2C7FE-7871-41C6-86AB-E2E44C2FA6ED" srsName="EPSG:25832">
          <gml:posList>572971.6625 5933062.6196 572981.0389 5933057.8931 572967.6565 5933030.8269 
572966.5346 5933031.2787 572957.0376 5933011.9002 572942.8373 5932982.9248 
572939.393 5932984.848 572937.002 5932980.589 572929.449 5932984.333 
572894.532 5933001.64 572887.5433 5933005.0321 572892.413 5933014.7603 
572859.1516 5933031.4102 572863.1502 5933039.4201 572831.7714 5933055.0844 
572835.349 5933062.2398 572831.2545 5933064.2838 572836.5152 5933075.0698 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="Gml_C0788E80-F5C3-4F59-9C49-D1D9D9F86082">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572836.5152 5933007.9276</gml:lowerCorner>
          <gml:upperCorner>572971.6625 5933075.0698</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_B8B4B56E-4E2C-42F0-A1F3-6EFBECC61E45" srsName="EPSG:25832">
          <gml:posList>572836.5152 5933075.0698 572877.6606 5933054.53 572908.488 5933039.141 
572900.999 5933024.105 572905.7417 5933022.2713 572939.8134 5933009.098 
572942.8407 5933007.9276 572947.15 5933016.7206 572946.5497 5933017.1799 
572955.7646 5933036.429 572954.0956 5933037.2432 572960.7754 5933050.7532 
572961.695 5933050.298 572963.4637 5933049.4218 572963.857 5933049.227 
572964.808 5933048.7562 572971.6625 5933062.6196 </gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_StrassenVerkehrsFlaeche gml:id="Gml_9F8132C9-585F-498A-9341-7C94E24180A9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572769.5767 5932947.209</gml:lowerCorner>
          <gml:upperCorner>573006.5734 5933173.5017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_98CF7AAD-C909-41D7-BDEC-95E8B01394AB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572778.71 5933087.887 572784.683 5933083.627 572786.552 5933082.295 
572788.302 5933081.048 572796.3144 5933075.1873 572804.411 5933069.265 
572818.853 5933057.868 572862.614 5933023.337 572873.638 5933014.638 
572883.046 5933007.215 572894.532 5933001.64 572929.449 5932984.333 
572937.002 5932980.589 572939.393 5932984.848 572942.8373 5932982.9248 
572966.5346 5933031.2787 572967.6565 5933030.8269 572984.608 5933065.1117 
572983.571 5933068.375 572975.641 5933072.619 572976.073 5933073.475 
572957.92 5933083.197 572939.732 5933092.94 572906.145 5933110.93 
572900.922 5933113.727 572856.8 5933137.336 572847.111 5933142.519 
572845.342 5933143.467 572813.34 5933159.839 572801.015 5933136.598 
572798.5743 5933132.4632 572790.0917 5933137.683 572788.0455 5933140.1586 
572786.4289 5933142.6342 572785.4605 5933144.5749 572784.703 5933146.621 
572784.492 5933148.2337 572784.5936 5933149.6355 572784.9215 5933151.0373 
572794.2578 5933170.214 572795.8548 5933173.5017 572799.7758 5933171.3889 
572878.208 5933129.1596 572989.6338 5933070.213 573006.5734 5933061.2509 
572994.336 5933040.0632 572957.6953 5932963.8529 572951.079 5932950.0947 
572949.7278 5932947.209 572912.4143 5932965.1798 572837.3411 5933022.9406 
572793.6617 5933055.9478 572769.5767 5933072.5548 572778.71 5933087.887 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <nutzungsform>2000</nutzungsform>
    </BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_SpezielleBauweise gml:id="Gml_A781C43F-2A86-46CA-B5A3-99F71378B31C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572865.0875 5933026.2989</gml:lowerCorner>
          <gml:upperCorner>572881.3845 5933054.53</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_41989044-46DE-45D0-8E84-942D145759C1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572865.0875 5933028.4388 572877.6606 5933054.53 572881.3845 5933052.671 
572869.3623 5933026.2989 572865.0875 5933028.4388 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <typ>1000</typ>
    </BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <BP_Wegerecht gml:id="Gml_71DBC293-17DE-45EF-BF31-E9EE0E6A075D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572938.0104 5933052.4571</gml:lowerCorner>
          <gml:upperCorner>572959.3777 5933085.1979</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <refTextInhalt xlink:href="#Gml_776BD1F9-6319-4335-9096-AA5CAF5B1E16" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_47BEB4C2-D607-4F0F-8132-7AACD5E82F7F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572959.3777 5933082.4163 572943.0375 5933052.4571 572938.0104 5933055.3014 
572954.1847 5933085.1979 572959.3777 5933082.4163 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <typ>3000</typ>
    </BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <BP_SpezielleBauweise gml:id="Gml_1999096A-1103-4DF8-AA4E-3BCEC07DE494">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572946.8473 5933068.8934</gml:lowerCorner>
          <gml:upperCorner>572957.723 5933082.1476</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_95DD7047-941A-4D8C-9531-2F6C09B776FB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572957.723 5933079.3824 572952.0021 5933068.8934 572946.8473 5933071.6355 
572952.5345 5933082.1476 572957.723 5933079.3824 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <typ>1000</typ>
    </BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <BP_NebenanlagenFlaeche gml:id="Gml_AC8E798A-DEA1-4F1C-8546-E6C5034A1103">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572827.505 5933104.5811</gml:lowerCorner>
          <gml:upperCorner>572865.9249 5933142.519</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>0</ebene>
      <refTextInhalt xlink:href="#Gml_F17C05E5-F1F4-40BD-86CD-2B0A4432B657" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:Polygon gml:id="Gml_8E6E814E-E71E-4FCA-B5BF-13B21A76007F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572865.9249 5933132.4534 572851.3696 5933104.5811 572842.292 5933110.058 
572830.939 5933116.908 572827.505 5933118.985 572830.506 5933119.786 
572833.767 5933122.237 572835.966 5933125.837 572839.0525 5933131.64 
572840.985 5933135.2731 572842.4886 5933138.1 572844.414 5933141.72 
572847.111 5933142.519 572848.1262 5933141.9759 572849.5806 5933141.1979 
572851.7337 5933140.0461 572851.9323 5933139.9399 572856.8 5933137.336 
572865.9249 5933132.4534 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <zweckbestimmung>1000</zweckbestimmung>
    </BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_StrassenbegrenzungsLinie gml:id="Gml_E3715B1E-DF0F-496A-BC89-DCFED9DCBA59">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572778.71 5932980.589</gml:lowerCorner>
          <gml:upperCorner>572984.608 5933159.839</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#Gml_963B9622-6285-42AC-B231-296F65C370E3" />
      <position>
        <gml:LineString gml:id="Gml_FE0E186E-B7E6-404F-9A1F-45036C04CD49" srsName="EPSG:25832">
          <gml:posList>572778.71 5933087.887 572784.683 5933083.627 572786.552 5933082.295 
572788.302 5933081.048 572796.3144 5933075.1873 572804.411 5933069.265 
572818.853 5933057.868 572862.614 5933023.337 572873.638 5933014.638 
572883.046 5933007.215 572894.532 5933001.64 572929.449 5932984.333 
572937.002 5932980.589 572939.393 5932984.848 572942.8373 5932982.9248 
572966.5346 5933031.2787 572967.6565 5933030.8269 572984.608 5933065.1117 
572983.571 5933068.375 572975.641 5933072.619 572976.073 5933073.475 
572957.92 5933083.197 572939.732 5933092.94 572906.145 5933110.93 
572900.922 5933113.727 572856.8 5933137.336 572847.111 5933142.519 
572845.342 5933143.467 572813.34 5933159.839 572801.015 5933136.598 
572798.5743 5933132.4632 </gml:posList>
        </gml:LineString>
      </position>
    </BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <XP_RasterplanBasis gml:id="Gml_B47BCDBE-AB8E-40C6-AD2C-895AE9E7807E">
      <refScan>
        <XP_ExterneReferenz>
          <georefURL>Billstedt28.pgw</georefURL>
          <referenzURL>Billstedt28.png</referenzURL>
        </XP_ExterneReferenz>
      </refScan>
    </XP_RasterplanBasis>
  </gml:featureMember>
</XPlanAuszug>
